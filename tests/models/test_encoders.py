import pytest
import torch  # type: ignore
from torch.nn import Module, Sequential, LeakyReLU
from einops.layers.torch import Rearrange
from src.models.encoders import (
    BirdNet,
    ResidualBlock,
    ResidualBottleneck,
    ResNet,
    VGGish,
)


@pytest.mark.parametrize("batch_size", [2])
@pytest.mark.parametrize("num_frames", [20])
def test_residual_encoder(
    batch_size: int,
    num_frames: int
) -> None:
    x = torch.randn(batch_size, 32, 192, 64)
    encoder = ResNet(
        width=4,
        depth=3,
        block_sizes=[8, 16, 32, 64, 128]
    )
    with torch.no_grad():
        z, _ = encoder(x)
    assert z.shape == (batch_size, 512, 12, 4)


@pytest.mark.parametrize("batch_size", [2])
def test_residual_block(
    batch_size: int
) -> None:
    x = torch.randn(2, 1, 96, 64)
    block1 = ResidualBlock(1, 32)
    block2 = ResidualBlock(32, 64)
    with torch.no_grad():
        x1 = block1(x)
        x2 = block2(x1)
    assert x1.size(1) == 32
    assert x2.size(1) == 64


@pytest.mark.parametrize("batch_size", [2])
def test_residual_bottleneck(
    batch_size: int
) -> None:
    x = torch.randn(2, 1, 96, 64)
    block1 = ResidualBottleneck(1, 32)
    block2 = ResidualBottleneck(32, 64)
    with torch.no_grad():
        x1 = block1(x)
        x2 = block2(x1)
    assert x1.size(1) == 32
    assert x1.size(2) == 48
    assert x1.size(3) == 32
    assert x2.size(1) == 64
    assert x2.size(2) == 24
    assert x2.size(3) == 16


def test_birdnet() -> None:
    bs, ts, fq, hl, ld = 2, 7296, 64, 12, 128
    x = torch.randn(bs, 1, ts, fq)
    encoder = BirdNet(frame_hop_length=hl, out_features=ld, out_channels=1)
    fr = (ts // 2**(len(encoder.features.blocks) + 1)) // hl
    with torch.no_grad():
        z = encoder(x)
    assert z.shape == (bs, fr, ld)
