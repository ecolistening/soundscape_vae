import pytest
import yaml
from pathlib import Path
from lightning import Trainer
from torch.nn import Sequential

@pytest.fixture
def root() -> Path:
    return Path.home() / "Data"

@pytest.fixture
def trainer() -> Trainer:
    return Trainer(fast_dev_run=True, devices=1, accelerator="cpu")
