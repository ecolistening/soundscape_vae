import librosa
import pytest
import torch
import torchaudio

from einops.layers.torch import Rearrange
from matplotlib import pyplot as plt
from librosa import display as libd
from pathlib import Path
from torch import Tensor
from torch.nn import Sequential, ModuleList

from src.transforms.spectrogram import Spectrogram
from src.transforms.frame import Frame

fixtures_path = Path().resolve() / "tests" / "fixtures"

@pytest.mark.parametrize('signal, transform, expected', [(
    Tensor([
        [0], [1], [2],
        [3], [4], [5],
        [6], [7], [8],
        [9], [10], [11]
    ]),
    Frame(window_length=3, hop_length=2),
    Tensor([
        [[0], [1], [2]],
        [[2], [3], [4]],
        [[4], [5], [6]],
        [[6], [7], [8]],
        [[8], [9], [10]]
    ]),
)])
def test_frame_1d(
    signal: Tensor,
    transform: Frame,
    expected: Tensor,
) -> None:
    framed = transform.forward(signal)
    assert (framed == expected).all()


@pytest.mark.parametrize('signal, transform, expected', [(
    Tensor([
        [[0], [12]],
        [[1], [13]],
        [[2], [14]],
        [[3], [15]],
        [[4], [16]],
        [[5], [17]],
        [[6], [18]],
        [[7], [19]],
        [[8], [20]],
        [[9], [21]],
        [[10], [22]],
        [[11], [23]],
    ]),
    Frame(window_length=3, hop_length=2),
    Tensor([
        [[[0], [12]], [[1], [13]], [[2],  [14]]],
        [[[2], [14]], [[3], [15]], [[4],  [16]]],
        [[[4], [16]], [[5], [17]], [[6],  [18]]],
        [[[6], [18]], [[7], [19]], [[8],  [20]]],
        [[[8], [20]], [[9], [21]], [[10], [22]]],
    ])
)])
def test_frame_2d(
    signal: Tensor,
    transform: Frame,
    expected: Tensor,
) -> None:
    framed = transform.forward(signal)
    assert (framed == expected).all()


@pytest.mark.parametrize('signal, transform, expected', [(
    torch.randn(size=(1, int(16_000 * (60 * 0.96)))),
    Sequential(
        Spectrogram(
            sample_rate=16_000,
            window_length=int(16_000 * 0.025),
            hop_length=int(16_000 * 0.01),
            frequency_scale="mel",
            num_mel_bins=64
        ),
        Rearrange('ch ts fq -> ts fq ch'),
        Frame(
            window_length=int(0.96 * (1.0 / 0.01)),
            hop_length=int(0.96 * (1.0 / 0.01)),
        ),
        Rearrange('fr ts fq ch -> fr ch ts fq'),
    ),
    torch.Size((60, 1, 96, 64))
)])
def test_frame_vggish(
    signal,
    transform,
    expected,
) -> None:
    assert transform(signal).shape == expected


@pytest.mark.parametrize('signal, transform, expected', [(
    torch.randn(size=(228, 4, 512)),
    Frame(window_length=12, hop_length=12),
    torch.Size((19, 12, 4, 512)),
)])
def test_frame_birdnet(
    signal,
    transform,
    expected,
) -> None:
    assert transform(signal).shape == expected
