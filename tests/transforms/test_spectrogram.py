import pytest
import librosa
import torch
import numpy as np
from pathlib import Path
from torch import Tensor
from typing import Tuple
from torch.nn import Sequential
from src.transforms.spectrogram import Spectrogram


@pytest.mark.parametrize("signal, transform, expected", [(
    torch.randn(size=(1, int(16_000 * (60 * 0.96)))),
    Spectrogram(
        sample_rate=16_000,
        window_length=400,
        hop_length=160
    ),
    (0.025, 0.01, 240, 512, torch.Size((1, 5760, 257))),
), (
    torch.randn(size=(1, 48_000 * 60)),
    Spectrogram(
        sample_rate=48_000,
        window_length=512,
        hop_length=384
    ),
    (0.032 / 3, 0.008, 128, 512, torch.Size((1, 7501, 257))),
)])
def test_spectrogram_stft_seconds(
    signal: Tensor,
    transform: Spectrogram,
    expected: Tuple,
) -> None:
    stft_window_length_seconds, stft_hop_length_seconds, overlap, fft_length, shape = expected
    assert transform.stft_window_length_seconds == stft_window_length_seconds
    assert transform.stft_hop_length_seconds == stft_hop_length_seconds
    assert transform.overlap == overlap
    assert transform.fft_length == fft_length
    assert transform(signal).shape == shape


@pytest.mark.parametrize("signal, transform, expected", [(
    torch.randn(size=(1, int(16_000 * (60 * 0.96)))),
    Spectrogram(
        sample_rate=16_000,
        stft_window_length_seconds=0.025,
        stft_hop_length_seconds=0.01
    ),
    (400, 160, 240, 512, torch.Size((1, 5760, 257))),
), (
    torch.randn(size=(1, 48_000 * 60)),
    Spectrogram(
        sample_rate=48_000,
        stft_window_length_seconds=1e-2 + 2 * (1e-3 / 3),
        stft_hop_length_seconds=8e-3
    ),
    (512, 384, 128, 512, torch.Size((1, 7501, 257))),
)])
def test_spectrogram_stft_length(
    signal: Tensor,
    transform: Spectrogram,
    expected: Tuple[int, int],
) -> None:
    window_length, hop_length, overlap, fft_length, shape = expected
    assert transform.window_length == window_length
    assert transform.hop_length == hop_length
    assert transform.overlap == overlap
    assert transform.fft_length == fft_length
    assert transform(signal).shape == shape


@pytest.mark.parametrize("signal, transform, expected", [(
    torch.randn(size=(1, int(16_000 * (60 * 0.96)))),
    Spectrogram(
        sample_rate=16_000,
        window_length=400,
        hop_length=160,
        frequency_scale="mel",
        mel_scaling_factor=1127.0,
        mel_break_frequency_hz=700.0,
        hz_to_mel_base=np.e,
    ),
    torch.Size((1, 5760, 64)),
), (
    torch.randn(size=(1, 48_000 * 60)),
    Spectrogram(
        sample_rate=48_000,
        window_length=512,
        hop_length=384,
        frequency_scale="mel",
        mel_scaling_factor=4581.0,
        mel_break_frequency_hz=1750.0,
        hz_to_mel_base=10.0,
    ),
    torch.Size((1, 7501, 64)),
)])
def test_log_mel_spectrogram_stft_length(
    signal: Tensor,
    transform: Spectrogram,
    expected: Tuple[int, int],
) -> None:
    assert transform(signal).shape == expected


@pytest.mark.parametrize("signal, transform, expected", [(
    torch.randn(size=(1, int(16_000 * (60 * 0.96)))),
    Spectrogram(
        sample_rate=16_000,
        stft_window_length_seconds=0.025,
        stft_hop_length_seconds=0.01,
        frequency_scale="mel",
        mel_scaling_factor=1127.0,
        mel_break_frequency_hz=700.0,
        hz_to_mel_base=np.e,
    ),
    torch.Size((1, 5760, 64)),
), (
    torch.randn(size=(1, 48_000 * 60)),
    Spectrogram(
        sample_rate=48_000,
        stft_window_length_seconds=0.032 / 3,
        stft_hop_length_seconds=0.008,
        frequency_scale="mel",
        mel_scaling_factor=4581.0,
        mel_break_frequency_hz=1750.0,
        hz_to_mel_base=10.0,
    ),
    torch.Size((1, 7501, 64)),
)])
def test_log_mel_spectrogram_stft_seconds(
    signal: Tensor,
    transform: Spectrogram,
    expected: Tuple[int, int],
) -> None:
    assert transform(signal).shape == expected
