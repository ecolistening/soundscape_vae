import pytest
import torch
from torch import Tensor
from torch.nn import Module
from typing import (
    Tuple
)
from src.transforms.latent_representations import (
    Histogram,
)


class HistogramNaive:
    def __init__(
        self,
        z_min: float,
        z_max: float,
        num_bins: int,
    ) -> None:
        self.z_min = z_min
        self.z_max = z_max
        self.num_bins = num_bins
        self.epsilon = 1e-6

    def __call__(self, z: Tensor) -> Tensor:
        batch_size, num_frames, latent_dim = z.shape
        hist = torch.zeros(batch_size, self.num_bins, latent_dim)
        bins = torch.linspace(self.z_min, self.z_max, self.num_bins + 1)
        for i in range(batch_size):
            # iterate through known boundaries of feature space as discrete regions
            binned_features_over_time = torch.zeros(self.num_bins, latent_dim)
            for j in range(self.num_bins):
                # consider z at d, a single feature 'concept'
                for d in range(latent_dim):
                    # observe z at d over all time frames
                    feature_over_frames = torch.zeros(num_frames)
                    for t in range(num_frames):
                        # count number of features occurring within this bin across time
                        if (bins[j] < z[i, t, d]) & (z[i, t, d] <= bins[j + 1]):
                            feature_over_frames[t] = 1
                    # normalise feature count over frames by the number of time steps
                    binned_features_over_time[j, d] = feature_over_frames.sum() / num_frames
            hist[i, ...] = binned_features_over_time
        return torch.softmax((hist + self.epsilon).log(), dim=1)

@pytest.mark.parametrize("signal, transform, expected", [(
    (
        (mu := torch.randn(2, 60, 20)) +
        (sigma := torch.randn(2, 60, 20).exp()) *
        (epsilon := torch.randn(2, 60, 20))
    ),
    Histogram(z_min=0.0, z_max=6.0, num_bins=10),
    HistogramNaive(z_min=0.0, z_max=6.0, num_bins=10),
)])
def test_histogram(
    signal: Tensor,
    transform: Module,
    expected: Tuple,
) -> None:
    batch_size, num_frames, latent_dim = signal.shape
    hist = transform(signal)
    assert hist.shape == (batch_size, transform.num_bins, latent_dim)
    assert (hist == expected(signal)).all()
