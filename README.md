# Soundscape VAE

by Kieran A. Gibb. Supported by Ivor. J. A. Simpson, Alice Eldridge & Chris Sandom

## Abstract
Ecoacoustics is an emerging science that seeks to understand the role of sound in ecological processes. Passive acoustic monitoring is being used to collect vast quantities of soundscape audio recordings to study variations in acoustic community to monitor biodiversity. However extracting relevant information from soundscape recordings is non-trivial. Recent approaches to machine-learned acoustic features appear promising but are limited by inductive biases, crude temporal integration and few methods to interpret downstream inference. We introduce a more relevant inductive bias by training a self-supervised representation learning algorithm - a convolutional Variational Auto-encoder (VAE) - on acoustic survey data collected in terrestrial temperate and tropical ecozones across sites representing a gradient of habitat degradation.

![soundscape_vae_schematic](./assets/abstract_figure.jpg)

[Experiments](#analysis) explore hardware sensitivity and normalisation, temporal integration methods and downstream  model interpretability. For full details, see the paper by [K. Gibb, I. J. A. Simpson, A. Eldridge, C. J. Sandom,  "Towards interpretable learned representations for Ecoacoustics using variational autoencoding", doi:10.1101/2023.09.07.556690](https://doi.org/10.1101/2023.09.07.556690).

## Usage
We provide a pipeline for configuring and training models. This is done using configuration files to instantiate objects in the code, see the [tutorial](./docs/getting_started/TUTORIAL.md) for how to adjust configuration files to suit your needs.

Alternatively, pytorch modules from within the codebase can be imported and used more flexibly, see the [API documentation](./docs/api_documentation/README.md).

## Experiments
Analysis code for experiments conducted in [our paper](https://doi.org/10.1101/2023.09.07.556690) on a dataset collected in temperate and tropical ecozones across sites representing a gradient of habitat degradation.

1. [Hardware Bias & Attribute Vectors](./analysis/experiments/experiment_1-hardware_bias_and_attribute_vectors.ipynb) examines the VAE's sensitivity to differences in recorder hardware's frequency response and we demonstrate a simple linear transformation to mitigate the effect of hardware variance on the learned representation.
2. [Linear Interpolation along Site Degradation Gradient](./analysis/experiments/experiment_2-linear_interpolation_along_site_degradation_gradient.ipynb) investigates approaches to interpret downstream inference by interpolating across a linear classification boundary in latent feature space indicative of the habitat degradation gradient, mapping discriminative embeddings back to the spectro-temporal domain to observe how soundscape components change. Results suggest varying combinations of biophony, geophony and anthrophony are used to infer sites along a degradation gradient and increased sensitivity to periodic signals improves on previous research using time-averaged representations for site classification.
3. [Feature Occurrence for Temporal Integration](./analysis/experiments/experiment_3-feature_occurrence_for_temporal_integration.ipynb) advances temporal integration methods by encoding latent features as a distribution over time to preserve periodic signals.
4. [Audio Reconstruction](./analysis/experiments/experiment_4-audio_reconstruction.ipynb) examines the efficacy of using the [Griffin Lim algorithm](https://librosa.org/doc/latest/generated/librosa.griffinlim.html#librosa.griffinlim) for phase estimation of magnitude spectrograms under BirdNet's selected FFT parameters to reconstruct raw audio.
