from ranzen.hydra import Option
from torch.nn import Identity
from torchvision.transforms import Compose
from ecolistening_torch import (
    SoundingOutChorusDataModule,
    SoundingOutChorusVAEEmbeddingsDataModule,
    SomersetWildlandsDataModule,
)
from soundscape_vae.algorithm import (
    Algorithm,
    DatasetEncoder,
)
from soundscape_vae.callbacks import (
    Callback,
    SpectrogramLogger,
    VAELatentsToParquet,
)
from soundscape_vae.loggers import WandbLogger
from soundscape_vae.models import (
    FeatureEncoder,
    SummaryStats,
    VAE,
    VGGish,
)
from soundscape_vae.relays import EcolisteningRelay

if __name__ == '__main__':
    data_module_ops = [
        Option(SoundingOutChorusDataModule, name="sounding_out"),
        Option(SomersetWildlandsDataModule, name="somerset_wildlands"),
        Option(SoundingOutChorusVAEEmbeddingsDataModule, name="sounding_out_vae_embeddings"),
    ]
    model_ops = [
        Option(VGGish, name="vggish"),
        Option(VAE, name="vae"),
        Option(Identity, name="none"),
    ]
    algorithm_ops = [
        Option(Algorithm, name='base'),
        Option(DatasetEncoder, name='dataset_encoder'),
        Option(Identity, name="none"),
    ]
    callback_ops = [
        Option(Callback, name="none"),
        Option(SpectrogramLogger, name="spectrogram_logger"),
        Option(VAELatentsToParquet, name="vae_latents_to_parquet"),
    ]
    transform_ops = [
        Option(Compose, name="spectrogram"),
    ]
    logger_ops = [
        Option(WandbLogger, name="wandb"),
    ]
    EcolisteningRelay.with_hydra(
        root=".conf",
        algorithm=algorithm_ops,
        callback=callback_ops,
        data_module=data_module_ops,
        logger=logger_ops,
        model=model_ops,
        transforms=transform_ops,
        clear_cache=True,
    )
