# Tutorial

In order to train your own VAE using our codebase you'll need to complete the following steps:

1. Define a pytorch lightning data module (see [sounding_out_torch](https://gitlab.com/ecolistening/sounding_out_torch) as an example) that provides a data loader for the audio data.

2. Import the data loader into `main.py` as a new data module option

```python
from ecoacoustic_data_module_package impory EcoacousticDataModule

# ...
data_module_ops = [
    Option(EcoacousticDataModule, name="ecoacoustics"),
]
```

3. In `config/data_module`, create a new configuration file for your data module e.g. `ecoacoustics.yaml`

```yaml
---
defaults:
  # specify the name from main.py
  - /schema/data_module: ecoacoustics
  - _self_
# pytorch lightning data module parameters are adjustable here
training_mode: step
train_batch_size: 6
eval_batch_size: 6
val_prop: 0.2
test_prop: 0.2
num_workers: 48
seed: 42
persist_workers: false
pin_memory: true
stratified_sampling: false
instance_weighting: false
# specify the path to your data set as expected by your data module
root: /path/to/dataset
segment_len: 58.368
sample_rate: 48_000
target_attrs: null
# specify any pre-processing transformations here
train_transforms:
  _target_: torchvision.transforms.Compose
  # for example, here we parameterise the DFT
  transforms:
    - _target_: soundscape_vae.transforms.Spectrogram
      sample_rate: 48_000
      window_length: 512
      hop_length: 384
      frequency_scale: mel
      magnitude_scale: log
      num_mel_bins: 64
      min_hz: 150
      max_hz: 15_000
      mel_break_frequency_hz: 1750.0
      mel_scaling_factor: 4581.0
      hz_to_mel_base: 10
    - _target_: torchvision.transforms.CenterCrop
      size: [7296, 64]
test_transforms:
  _target_: torchvision.transforms.Compose
  # ensure we apply the same for test
  transforms:
    - _target_: soundscape_vae.transforms.Spectrogram
      sample_rate: 48_000
      window_length: 512
      hop_length: 384
      frequency_scale: mel
      magnitude_scale: log
      num_mel_bins: 64
      min_hz: 150
      max_hz: 15_000
      mel_break_frequency_hz: 1750.0
      mel_scaling_factor: 4581.0
      hz_to_mel_base: 10
    - _target_: torchvision.transforms.CenterCrop
      size: [7296, 64]
```

4. In `config/experiments`, create an experiment pipeline configuration file, adjusting the `override` to use your specified data module, e.g. `override /data_module: ecoacoustics`.

```yaml
# @package _global_

# usage: python main.py +experiment=ecoacoustics/soundscape_vae

defaults:
  - /schema/encoder: bird_net
  - /schema/decoder: inverse_bird_net
  - /schema/model: vae
  - /schema/callback: spectrogram_logger
  - /schema/persistence_strategy: parquet
  - override /algorithm: soundscape_autoencoder
  - override /data_module: ecoacoustics
  - _self_

train: true
predict: true
save: true

# specify a location to save your embeddings, and whether to upload to W&B
persistence_strategy:
  file_path: ~/data/embeddings/${ name }
  upload: ${ save }

# set the Pytorch Lightning Module class parameters (see ./soundscape_vae/algorithm/soundscape_autoencoder.py)
algorithm:
  learning_rate: 4.e-5
  optimiser_cls: torch.optim.AdamW
  scheduler_cls: torch.optim.lr_scheduler.CosineAnnealingLR
  scheduler_config:
    T_max: ${ trainer.max_steps }
    eta_min: 4.e-7

# set the VAE hyperparameters (see ./soundscape_vae/models/vae.py)
model:
  sigma_mode: global

# set the encoder hyperparameters (e.g. see ./soundscape_vae/models/encoders/bird_net.py)
encoder:
  width: 4
  depth: 3
  dropout_prob: 0.5
  in_channels: 1
  out_channels: 2
  out_features: 128
  weight_init_std: 1.e-2
  frame_hop_length: 6

# set the decoder hyperparameters (e.g. see ./soundscape_vae/models/decoders/inverse_bird_net.py)
decoder:
  width: 4
  depth: 3
  dropout_prob: 0.5
  in_features: 128
  out_channels: 1
  frame_hop_length: 6

# specify a pytorch lightning callback, for example, our spectrogram logger for uploading spectrogram images during training (see ./soundscape_vae/callbacks/spectrogram_logger.py)
callback:
  batch_limit: 10
  sample_rate: ${ data_module.sample_rate }
  window_length: 512
  hop_length: 384
  step_every: 500
  min_hertz: 150
  max_hertz: 15_000
  cmap: viridis

# specify mode checkpoint parameters (see pytorch lightning documentation)
checkpoint:
  monitor: val/loss
  mode: min

# specify pytorch lightning trainer parameters (see pytorch lightning documentation)
trainer:
  accelerator: gpu
  devices: 1
  max_steps: 100000
  max_epochs: -1
  log_every_n_steps: 50
  val_check_interval: 250
```

4. Execute the training pipeline **twice** as specified at the top of the config file (the first time generates the config file templates, the second and subsequent times will train the model).

```bash
python main.py +experiment=ecoacoustics/soundscape_vae
```
