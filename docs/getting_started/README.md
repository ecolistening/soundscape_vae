# Getting Started

This project is built using [Hydra](https://github.com/facebookresearch/hydra/) for initializing objects using configuration files, [Pytorch Lightning](https://lightning.ai/) for rapid pipeline development and [PyTorch](https://pytorch.org/) for model components. This allows us to easily instantiate different model configurations using config files that reference different components and specify model hyperparameters. Available model components are added in `main.py` under a namespace and are then accessible using that reference from configuration files defined in the `config` directory.

## Installation
1. Ensure you have a python version manager on your system (for example, [pyenv](https://github.com/pyenv/pyenv) or [conda](https://www.anaconda.com)) and python version 3.9.12 installed.

2. Update pip

```
pip install --upgrade pip
```

3. Install package manager and project dependencies

```
pip install poetry
poetry install
```

## Train a model

To train a model using the specifications from [our paper](), you can simply run the command:

```
python main.py +experiment=sounding_out_chorus/bird_net_vae
```

For further instructions, check out the [tutorial](./TUTORIAL.md).
