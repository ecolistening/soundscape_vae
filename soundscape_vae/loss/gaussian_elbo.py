from dataclasses import dataclass
from torch import Tensor
from torch.functional import F
from typing import (
    Any,
    Dict,
)

__all__ = ["GaussianELBO"]

@dataclass(frozen=True)
class GaussianELBO:
    def __call__(
        self,
        x: Tensor,
        mu_z: Tensor,
        log_sigma_z: Tensor,
        q_z: Tensor,
        **kwargs: Any
    ) -> Dict[str, Tensor]:
        nll = self.negative_log_likelihood(x, mu_z, log_sigma_z)
        dkl = self.kl_divergence(q_z)
        return dict(
            loss=nll + dkl,
            log_likelihood=-nll.detach(),
            kl_divergence=dkl.detach(),
            log_sigma_z=log_sigma_z,
            sigma_z=log_sigma_z.exp(),
        )

    def negative_log_likelihood(
        self,
        x: Tensor,
        mu_z: Tensor,
        log_sigma_z: Tensor
    ) -> Tensor:
        """reconstruction under a gaussian error"""
        nll = 1/2 * (log_sigma_z + (x - mu_z).pow(2) / log_sigma_z.exp())
        return nll.flatten(start_dim=1, end_dim=-1).sum(dim=1).mean(dim=0)

    def kl_divergence(
        self,
        q_z: Tensor
    ) -> Tensor:
        """q_z is modelled as a d-dimensional posterior distribution of latent variables z given data x"""
        mu_x, log_sigma_x = q_z.chunk(2, dim=-1)
        log_sigma_x = -6 + F.softplus(log_sigma_x + 6)
        kl_divergence = -1/2 * (1 + log_sigma_x - mu_x.pow(2) - log_sigma_x.exp())
        return kl_divergence.flatten(start_dim=1, end_dim=-1).sum(dim=1).mean(dim=0)
