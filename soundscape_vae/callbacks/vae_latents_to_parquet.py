import os
import einops
import pandas as pd
import torch
from conduit.data import TernarySample
from dataclasses import dataclass
from pathlib import Path
from lightning import (
    Callback,
    LightningModule,
    Trainer,
)
from tempfile import TemporaryDirectory
from torch import Tensor
from typing import (
    Any,
    Dict,
    List,
    Optional,
    Tuple,
)

__all__ = ['VAELatentsToParquet']


@dataclass(unsafe_hash=True)
class VAELatentsToParquet(Callback):
    file_path: str

    def __new__(cls, *args, **kwargs) -> object:
        obj = object.__new__(cls)
        Callback.__init__(obj)
        return obj

    def __post_init__(self) -> None:
        self.tmp = TemporaryDirectory()

    def on_predict_batch_end(
        self,
        trainer: Trainer,
        pl_module: LightningModule,
        outputs: Tuple[Tensor, Tensor, Dict[str, Tensor]],
        batch: TernarySample,
        batch_idx: int,
        dataloader_idx: int,
    ) -> str:
        batch_size = batch.x.size(0)
        index, dl_idx, batch_output = outputs
        q_z = einops.rearrange(batch_output["q_z"].cpu(),'(bs fr) ld -> bs fr ld', bs=batch_size)
        num_frames, latent_dim = q_z.size(1), q_z.size(2) // 2
        mu_columns = [rf"$\mu^{d}_{t}$" for t in range(num_frames) for d in range(latent_dim)]
        log_sigma_columns = [rf"$\log(\sigma^{d}_{t})$" for t in range(num_frames) for d in range(latent_dim)]
        data = pd.DataFrame(
            data=torch.hstack([dl_idx.cpu().unsqueeze(1), q_z.flatten(start_dim=1)]),
            columns=["train/val/test", *mu_columns, *log_sigma_columns],
            index=index.cpu().numpy(),
        )
        data["train/val/test"] = data["train/val/test"].astype(int)
        data.index.rename('file_i', inplace=True)
        file_name = f"features_{dataloader_idx}_{batch_idx}"
        data.to_parquet(str(Path(self.tmp.name) / file_name), engine="auto", index=True)
        return file_name

    def on_predict_epoch_end(
        self,
        trainer: Trainer,
        pl_module: LightningModule,
        outputs: List[List[str]],
        *args: Any,
        **kwargs: Any,
    ) -> None:
        pd.concat([
            pd.read_parquet(file_path)
            for batch_idx, file_path in batch_output
            for dataloader_idx, batch_output in outputs
        ], axis=0).to_parquet(
            str(Path(self.file_path).expanduser() / 'features.parquet'),
            engine="auto",
            index=True
        )
        self.tmp.cleanup()

    # def on_predict_epoch_end(
    #     self,
    #     trainer: Trainer,
    #     pl_module: LightningModule,
    #     predict_outputs: List[List[Tuple[Tensor, Tensor, Dict[str, Tensor]]]],
    #     *args: Any,
    #     **kwargs: Any,
    # ) -> None:
    #     idx = torch.cat([
    #         batch_output[0]
    #         for dataloader_outputs in predict_outputs
    #         for batch_output in dataloader_outputs
    #     ]).numpy()
    #     dataloader_idx = torch.cat([
    #         batch_output[1]
    #         for dataloader_outputs in predict_outputs
    #         for batch_output in dataloader_outputs
    #     ])
    #     q_z = torch.vstack([
    #         einops.rearrange(batch_output[2]["q_z"].cpu(), '(bs fr) ld -> bs fr ld', bs=batch_output[0].size(0))
    #         for dataloader_outputs in predict_outputs
    #         for batch_output in dataloader_outputs
    #     ])
    #     num_observations, num_frames, latent_dim = q_z.size(0), q_z.size(1), q_z.size(2) // 2
    #     mu_columns = [rf"$\mu^{d}_{t}$" for t in range(num_frames) for d in range(latent_dim)]
    #     log_sigma_columns = [rf"$\log(\sigma^{d}_{t})$" for t in range(num_frames) for d in range(latent_dim)]
    #     df = pd.DataFrame(
    #         data=torch.hstack([dataloader_idx.unsqueeze(1), q_z.flatten(start_dim=1)]),
    #         columns=["train/val/test", *mu_columns, *log_sigma_columns],
    #         index=idx
    #     )
    #     df["train/val/test"] = df["train/val/test"].astype(int)
    #     df.to_parquet("features.parquet", engine="auto", index=True)
