from dataclasses import dataclass
from typing import Optional

__all__ = ["ModelCheckpoint"]

@dataclass
class ModelCheckpoint:
    _target_: str = "pytorch_lightning.callbacks.ModelCheckpoint"
    dirpath: Optional[str] = None
    filename: Optional[str] = None
    monitor: Optional[str] = None
    verbose: bool = False
    save_last: Optional[str] = None
    save_top_k: int = 1
    save_weights_only: bool = False
    mode: str = 'min'
    auto_insert_metric_name: bool = True
    every_n_train_steps: Optional[int] = None
    # train_time_interval: Optional = None
    every_n_epochs: Optional[int] = None
    save_on_train_epoch_end: Optional[bool] = None
