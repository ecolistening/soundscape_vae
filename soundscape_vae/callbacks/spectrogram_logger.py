import librosa
import numpy as np
import matplotlib
import torch
import wandb

matplotlib.use("Agg")

from conduit.data.structures import TernarySample
from dataclasses import dataclass
from einops import repeat
from librosa import display as libd
from matplotlib import gridspec as gs
from matplotlib import pyplot as plt
from matplotlib.colors import Colormap
from matplotlib.figure import Figure
from matplotlib import cm
from pandas import DataFrame
from lightning import (
    LightningModule,
    Trainer
)
from torch import Tensor
from typing import (
    Any,
    Dict,
    List,
    Optional,
    Union,
)
from soundscape_vae.callbacks.base import Callback
from soundscape_vae.types import Stage

__all__ = ['SpectrogramLogger']

@dataclass(unsafe_hash=True)
class SpectrogramLogger(Callback):
    batch_limit: Optional[int] = None
    sample_rate: int = 48_000
    window_length: int = 512
    hop_length: int = 384
    min_hertz: Optional[float] = None
    max_hertz: Optional[float] = None
    y_scale: str = 'mel'
    cmap: Union[str, Colormap] = 'viridis'
    train_log_every_n_steps: Optional[int] = None
    val_log_every_n_steps: Optional[int] = None

    def on_train_batch_end(self, trainer: Trainer, pl_module: LightningModule, *args: Any, **kwargs: Any) -> None:
        if self.train_log_every_n_steps and (trainer.global_step % self.train_log_every_n_steps) == 0:
            step_outputs = pl_module.training_step_outputs[0]
            figs = self.step(step_outputs)
            for fig in figs:
                pl_module.logger.experiment.log({ f"train/spectrogram": wandb.Image(fig) })
                plt.close(fig)
        pl_module.training_step_outputs.clear()

    def on_validation_batch_end(self, trainer: Trainer, pl_module: LightningModule, *args: Any, **kwargs: Any) -> None:
        if self.val_log_every_n_steps and (trainer.global_step % self.val_log_every_n_steps) == 0:
            step_outputs = pl_module.validation_step_outputs[0]
            figs = self.step(step_outputs)
            for fig in figs:
                pl_module.logger.experiment.log({ f"val/spectrogram": wandb.Image(fig) })
                plt.close(fig)
        pl_module.validation_step_outputs.clear()

    def step(self, step_outputs: Dict[str, Tensor]) -> List[Figure]:
        # extract target and approximation from step outputs
        xs = step_outputs["x"].cpu()
        mu_zs = step_outputs["mu_z"].cpu()
        sigma_zs = step_outputs["log_sigma_z"].cpu()
        # sample randomly from the batch to render
        sample_idx = torch.randperm(xs.size(0))[:(self.batch_limit or xs.size(0))]
        figs = []
        for i in sample_idx:
            x = librosa.amplitude_to_db(xs[i, 0].exp().t().numpy())
            mu_z = librosa.amplitude_to_db(mu_zs[i, 0].exp().t().numpy())
            x_max = max(x.max(), mu_z.max())
            x_min = min(x.min(), mu_z.min())
            spectrograms = [x, mu_z]
            # if pixelwise log sigma available
            spectrograms = spectrograms + [sigma_zs[i, 1]].exp() if mu_zs.shape == sigma_zs.shape else spectrograms
            # setup figure
            fig = plt.figure(figsize=(11, 2 * len(spectrograms)), dpi=200)
            grid_spec = gs.GridSpec(
                nrows=len(spectrograms),
                ncols=2,
                width_ratios=[1, 0.04],
                hspace=0.8
            )
            # plot both to figure
            for j, spectrogram in enumerate(spectrograms):
                mesh = libd.specshow(
                    spectrogram,
                    sr=self.sample_rate,
                    hop_length=self.hop_length,
                    win_length=self.window_length,
                    n_fft=np.power(2, int(np.ceil(np.log(self.window_length) / np.log(2.0)))),
                    y_axis=self.y_scale,
                    x_axis="time",
                    vmin=x_min, vmax=x_max,
                    fmin=self.min_hertz, fmax=self.max_hertz,
                    ax=fig.add_subplot(grid_spec[j, 0]),
                    cmap=self.cmap,
                )
                plt.colorbar(
                    mesh,
                    format='%+3.1f dB',
                    cax=fig.add_subplot(grid_spec[j, 1]),
                    orientation="vertical"
                )
            figs.append(fig)
        return figs
