from dataclasses import dataclass
from lightning.pytorch import (
    Callback as LightningCallback,
    LightningModule,
    Trainer
)
from typing import Any

__all__ = ["Callback"]

@dataclass(unsafe_hash=True)
class Callback(LightningCallback):
    def __new__(cls, *args, **kwargs) -> object:
        obj = object.__new__(cls)
        LightningCallback.__init__(obj)
        return obj

    def on_train_batch_end(
        self,
        trainer: Trainer,
        pl_module: LightningModule,
        *args: Any,
        **kwargs: Any
    ) -> None:
        pl_module.training_step_outputs.clear()

    def on_validation_batch_end(
        self,
        trainer: Trainer,
        pl_module: LightningModule,
        *args: Any,
        **kwargs: Any
    ) -> None:
        pl_module.training_step_outputs.clear()
