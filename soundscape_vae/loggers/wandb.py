from dataclasses import dataclass
from typing import Any, Optional

@dataclass
class WandbLogger:
    _target_: str = "lightning.pytorch.loggers.wandb.WandbLogger"
    name: Optional[str] = None
    save_dir: str = ".wandb"
    offline: Optional[bool] = False
    id: Optional[str] = None
    anonymous: Optional[bool] = None
    version: Optional[str] = None
    project: Optional[str] = None
    log_model: Optional[bool] = True
    experiment: Any = None
    prefix: Optional[str] = None
    group: Optional[str] = None
    entity: Optional[str] = None
    reinit: bool = False
