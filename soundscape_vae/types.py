from enum import Enum, auto

__all__ = [
    "Stage"
]

class Stage(Enum):
    train = auto()
    val = auto()
    test = auto()
