import torch
from dataclasses import dataclass
from torch import Tensor
from torch.nn import Module

__all__ = ["frame", "unframe", "Frame", "Unframe"]

def frame(
    x: Tensor,
    window_length: int,
    hop_length: int
) -> Tensor:
    """
    Expand an nD tensor to an nD+1 tensor using a windowing function

    - Calculate the number of frames according to the window, ensuring
      the tail is removed when the size of the frames doesn't match
    - Size switches the old dimension, 'num_samples', to two new dimensions,
      'num_frames' and 'window_length'
    - From existing strides, to get to the next frame, step by the stride
      times the hop length, keep existing strides
    """
    num_samples, *other_dims = x.shape
    num_frames = 1 + int(round((num_samples - window_length) // hop_length))
    return x.as_strided(
        size=(num_frames, window_length, *other_dims),
        stride=(x.stride()[0] * hop_length, *x.stride())
    )


def unframe(
    x: Tensor,
    window_length: int,
    hop_length: int
) -> Tensor:
    """
    Reduce an nD tensor to an nD-1 tensor using a windowing function

    - Original number of samples is unavailable, we cannot know how many
      were discarded (within this scope), use what we know to remove the overlap
    - Calculate the difference between the window length and the hop length 
      times the number of frames - 1
    """
    num_frames, window_length, *other_dims = x.shape
    num_samples = (num_frames * window_length) - int((num_frames - 1) * (window_length - hop_length))
    return x.as_strided(
        size=(num_samples, *other_dims),
        stride=x.stride()[1:]
    )


@dataclass(frozen=True)
class Frame:
    window_length: int
    hop_length: int

    def __call__(self, x: Tensor) -> Tensor:
        return frame(x, window_length=self.window_length, hop_length=self.hop_length)


@dataclass(frozen=True)
class Unframe:
    window_length: int
    hop_length: int

    def __call__(self, x: Tensor) -> Tensor:
        return unframe(x, window_length=self.window_length, hop_length=self.hop_length)
