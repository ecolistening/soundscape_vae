import librosa
import numpy as np
import torch

from dataclasses import dataclass
from typing import Optional, Union
from torch import Tensor
from torch.nn import Module

from soundscape_vae.utils import (
    frame_to_seconds,
    seconds_to_frame,
    fft_length,
    hertz_to_mel,
)

__all__ = ["Spectrogram"]

@dataclass(frozen=True)
class Spectrogram:
    sample_rate: int
    window_length: int
    hop_length: int
    frequency_scale: str = "linear"
    mel_scaling_factor: Optional[float] = 1127.0
    mel_break_frequency_hz: Optional[float] = 700.0
    hz_to_mel_base: Optional[float] = np.e
    num_mel_bins: Optional[int] = 64
    min_hz: Optional[float] = 125.0
    max_hz: Optional[float] = 7500.0
    top_db: Optional[float] = 100.0
    log_offset: Optional[float] = 1e-3
    magnitude_scale: Optional[str] = None

    @property
    def nyquist_hz(self):
        return self.sample_rate / 2.0

    @property
    def fft_length(self):
        return 2 ** int(np.ceil(np.log(self.window_length) / np.log(2.0)))

    def __call__(self, data: Tensor) -> Tensor:
        signal = data.flatten().numpy()
        # compute discrete fourier transform using librosa's FFT implementation
        fft = librosa.stft(
            signal,
            win_length=self.window_length,
            hop_length=self.hop_length,
            n_fft=self.fft_length,
            window="hann",
        )
        # extract the magnitude, discard phase
        magnitude, _ = librosa.magphase(fft)
        # if we want the spectrogram in the mel scale, i.e. adjusted according to
        # perceptual scale for equal spacing of pitches, we apply a logarithmic
        # transformation of spectrogram frequency bins using a mel filterbank matrix
        num_spectrogram_bins = magnitude.shape[0]
        if self.frequency_scale == "mel":
            mel_filterbank = self.mel_filterbank(num_spectrogram_bins)
            mel_spectrogram = (magnitude.transpose(1, 0) @ mel_filterbank).transpose(1, 0)
            magnitude = mel_spectrogram
        # apply magnitude scaling
        if self.magnitude_scale == "db":
            # shift from magnitude to power spectrum
            magnitude = magnitude ** 2
            # convert magnitude spectrum to dB scale (compute dB relative to peak power)
            magnitude = 10.0 * np.log10(np.maximum(self.log_offset, magnitude))
            magnitude = np.maximum(magnitude, magnitude.max() - self.top_db)
        # for log mel spectrograms
        elif self.magnitude_scale == "log":
            magnitude = np.log(np.maximum(self.log_offset, magnitude))
        # remap to tensor on device with added channel dimension
        magnitude = torch.as_tensor(magnitude.transpose(1, 0), device=data.device).unsqueeze(0)
        # return the phase along second channel if requested
        # if self.return_phase and self.frequency_scale != "mel":
        #     phase = torch.as_tensor(np.angle(phase).transpose(1, 0), device=data.device).unsqueeze(0)
        #     return torch.cat([magnitude, phase], dim=0)
        # otherwise we just return the magnitude
        return magnitude

    def mel_filterbank(self, num_spectrogram_bins: int):
        spectrogram_bins_hertz = np.linspace(0.0, self.nyquist_hz, num_spectrogram_bins)
        min_mel, max_mel = hertz_to_mel(
          np.array([self.min_hz, self.max_hz]),
          scaling_factor=self.mel_scaling_factor,
          break_frequency_hz=self.mel_break_frequency_hz,
          log=lambda x: np.log(x) / np.log(self.hz_to_mel_base),
        )
        spectrogram_bins_mel = hertz_to_mel(
          spectrogram_bins_hertz,
          scaling_factor=self.mel_scaling_factor,
          break_frequency_hz=self.mel_break_frequency_hz,
          log=lambda x: np.log(x) / np.log(self.hz_to_mel_base),
        )
        # The i'th mel band (starting from i=1) has center frequency
        # mel_bands[i], lower edge mel_bands[i-1], and higher edge
        # mel_bands[i+1].  Thus, we need num_mel_bins + 2 values in
        # the mel_bands arrays.
        mel_bands = np.linspace(min_mel, max_mel, self.num_mel_bins + 2)
        filter_bank = np.empty((num_spectrogram_bins, self.num_mel_bins))
        for i in range(self.num_mel_bins):
            lower_edge_mel, center_mel, upper_edge_mel = mel_bands[i:i + 3]
            # calculate lower and upper slopes for every spectrogram bin.
            # line segments are linear in the *mel* domain, not hertz.
            lower_slope = ((spectrogram_bins_mel - lower_edge_mel) / (center_mel - lower_edge_mel))
            upper_slope = ((upper_edge_mel - spectrogram_bins_mel) / (upper_edge_mel - center_mel))
            # then intersect them with each other and zero.
            filter_bank[:, i] = np.maximum(0.0, np.minimum(lower_slope, upper_slope))
        # HTK excludes the spectrogram DC bin; make sure it always gets a zero coefficient.
        filter_bank[0, :] = 0.0
        return filter_bank

    # TODO: refactor into @property
    def _calculate_window(
        self,
        window_length: Optional[int],
        stft_window_length_seconds: Optional[float],
    ) -> None:
        if stft_window_length_seconds is not None:
            assert isinstance(stft_window_length_seconds, float), "'stft_window_length_seconds' must be a float"
            self.stft_window_length_seconds = stft_window_length_seconds
            self.window_length = seconds_to_frame(stft_window_length_seconds, self.sample_rate)
        elif window_length is not None:
            assert isinstance(window_length, int), "'window_length' must be an integer"
            self.stft_window_length_seconds = frame_to_seconds(window_length, self.sample_rate)
            self.window_length = window_length
        else:
            raise ValueError("size or duration of window must be specified")

    def _calculate_hop(
        self,
        hop_length: Optional[int],
        stft_hop_length_seconds: Optional[float],
        overlap: Optional[Union[float, int]],
    ) -> None:
        if overlap is not None:
            if isinstance(overlap, float):
                assert 0 <= overlap <= 1, "'overlap' must be between 0 and 1"
                self.stft_hop_length_seconds = (
                    self.stft_window_length_seconds - (self.stft_window_length_seconds * overlap)
                )
                self.overlap = int(self.window_length * overlap)
                self.hop_length = self.window_length - self.overlap
            elif isinstance(overlap, int):
                assert overlap <= self.window_length, "'hop length' greater than window length not supported"
                self.hop_length = self.window_length - overlap
                self.stft_hop_length_seconds = frame_to_seconds(self.hop_length, self.sample_rate)
                self.overlap = overlap
            else:
                raise ValueError("'overlap' must be a float or integer")
        else:
            if stft_hop_length_seconds is not None:
                self.stft_hop_length_seconds = stft_hop_length_seconds
                self.hop_length = seconds_to_frame(stft_hop_length_seconds, self.sample_rate)
            elif hop_length is not None:
                self.stft_hop_length_seconds = frame_to_seconds(hop_length, self.sample_rate)
                self.hop_length = hop_length
            else:
                raise ValueError("size or duration of hop must be specified")
            self.overlap = self.window_length - self.hop_length
