import numpy as np
import torch
from torch import Tensor
from torch.nn import Module
from typing import (
    Optional
)

__all__ = [
    "GaussianKernel",
    "KernelDensityEstimator",
]

class GaussianKernel(Module):
    def __init__(
        self,
        bandwidth: float = 1.0
    ) -> None:
        super().__init__()
        self.bandwidth = bandwidth

    def _diffs(
        self,
        test_data: Tensor,
        train_data: Tensor,
    ) -> Tensor:
        test_data = test_data.view(test_data.shape[0], 1, *test_data.shape[1:])
        train_data = train_data.view(1, train_data.shape[0], *train_data.shape[1:])
        return test_data - train_data

    def forward(
        self,
        test_data: Tensor,
        train_data: Tensor,
    ) -> Tensor:
        n, d = train_data.shape
        n, h = torch.tensor(n, dtype=torch.float32), torch.tensor(self.bandwidth)
        pi = torch.tensor(np.pi)
        Z = 0.5 * d * torch.log(2 * pi) + d * torch.log(h) + torch.log(n)
        diffs = self._diffs(test_data, train_data) / h
        log_exp = -0.5 * torch.norm(diffs, p=2, dim=-1) ** 2
        return torch.logsumexp(log_exp - Z, dim=-1)

    @torch.no_grad()
    def sample(
        self,
        train_data: Tensor
    ) -> Tensor:
        device = train_data.device
        epsilon = torch.randn(train_data.shape, device=device) * self.bandwidth
        return train_data + epsilon

class KernelDensityEstimator(Module):
    """
    Implementation of Kernel Density Estimation (KDE)

    Taken from:
    https://github.com/EugenHotaj/pytorch-generative/blob/master/pytorch_generative/models/kde.py

    Kernel density estimation is a nonparametric density estimation method. It works by
    placing kernels K on each point in a "training" dataset D. Then, for a test point x,
    p(x) is estimated as p(x) = 1 / |D| \sum_{x_i \in D} K(u(x, x_i)), where u is some
    function of x, x_i. In order for p(x) to be a valid probability distribution, the kernel
    K must also be a valid probability distribution.
    """
    def __init__(
        self,
        train_data: Tensor,
        kernel: Optional[Module] = None,
    ) -> None:
        super().__init__()
        self.kernel = kernel or GaussianKernel()
        self.train_data = train_data
        assert len(self.train_data.shape) == 2, "Input cannot have more than two axes."

    @property
    def device(self):
        return self.train_data.device

    def forward(self, x: Tensor) -> Tensor:
        return self.kernel(x, self.train_data)

    @torch.no_grad()
    def sample(self, num_samples: int) -> Tensor:
        idxs = np.random.choice(range(len(self.train_data)), size=num_samples)
        return self.kernel.sample(self.train_data[idxs])
