import torch

from dataclasses import dataclass
from torch import Tensor
from torch.nn import Module

from soundscape_vae.transforms.kernel_density_estimator import (
    GaussianKernel,
    KernelDensityEstimator,
)

__all__ = ["Histogram", "FeatureOccurence"]

@dataclass(frozen=True)
class Histogram:
    z_min: float
    z_max: float
    num_bins: int
    epsilon: float = 1e-6

    def __call__(self, z: Tensor) -> Tensor:
        batch_size, num_frames, latent_dim = z.shape
        hist = torch.zeros(batch_size, self.num_bins, latent_dim)
        bins = torch.linspace(self.z_min, self.z_max, self.num_bins + 1)
        for j in range(self.num_bins):
            hist[:, j, ...] = ((bins[j] < z) & (z < bins[j + 1])).sum(axis=1) / num_frames
        return torch.softmax((hist + self.epsilon).log(), dim=1)


@dataclass(frozen=True)
class FeatureOccurence:
    z_min: float
    z_max: float
    num_samples: int
    bandwidth: float = 1.0
    epsilon: float = 1e-6

    def __call__(self, z: Tensor) -> Tensor:
        batch_size, num_frames, latent_dim = z.shape
        log_probs = torch.zeros(batch_size, self.num_samples, latent_dim, device=z.device)
        samples = torch.linspace(self.z_min, self.z_max, self.num_samples).view(-1, 1)
        for i in range(batch_size):
            log_probs[i] = torch.stack([
                KernelDensityEstimator(
                    kernel=GaussianKernel(bandwidth=self.bandwidth),
                    train_data=z[i, :, [j]]
                ).forward(samples)
                for j in range(latent_dim)
            ], dim=1)
        # add some epsilon to ensure no zero probs
        probs = torch.maximum(log_probs.exp(), torch.tensor(self.epsilon))
        # normalise w softmax
        return torch.softmax(probs.log(), dim=1)
