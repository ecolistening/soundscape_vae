import torch
import wandb

from enum import Enum, auto
from conduit.data import TernarySample
from conduit.data.datamodules import CdtDataModule
from dataclasses import dataclass
from hydra.utils import instantiate
from omegaconf import DictConfig
from pandas import DataFrame
from pathlib import Path
from lightning import (
    LightningModule,
    Trainer,
)
from torch import Tensor
from torch.nn import Module
from torch.optim import Optimizer
from typing import (
    Any,
    Callable,
    Dict,
    List,
    Optional,
    Tuple,
)
from soundscape_vae.utils import (
    prefix_keys,
    detach_values,
    cpu_values,
)

from soundscape_vae.types import Stage

__all__ = ["Algorithm"]

@dataclass(unsafe_hash=True)
class Algorithm(LightningModule):
    model: Module
    learning_rate: float = 1e-4
    loss_cls: str = None
    loss_config: Optional[DictConfig] = None
    optimiser_cls: str = "torch.optim.AdamW"
    optimiser_config: Optional[DictConfig] = None
    scheduler_cls: Optional[str] = None
    scheduler_config: Optional[DictConfig] = None
    scheduler_interval: str = "step"
    scheduler_frequency: int = 1
    batch_transforms: Optional[List[Callable]] = None

    @classmethod
    def from_artefact(
        cls,
        artefact_id: str,
        save_dir: Optional[str] = str(Path().resolve()),
    ) -> "Algorithm":
        api = wandb.Api()
        artefact = api.artifact(artefact_id)
        artefact_dir = artefact.download(root=save_dir)
        artefact = torch.load(str(Path(artefact_dir) / "model.pt"))
        state_dict, config = artefact.values()
        algorithm = instantiate(config["algorithm"], model=config["model"])
        algorithm.load_state_dict(state_dict)
        return algorithm

    def __new__(cls, *args: Any, **kwargs: Any):
        obj = object.__new__(cls)
        LightningModule.__init__(obj)
        return obj

    def __post_init__(self):
        self.loss = instantiate(DictConfig(dict(
            _target_=self.loss_cls,
            **(self.loss_config or {})
        ))) if self.loss_cls is not None else None
        self.training_step_outputs = []
        self.validation_step_outputs = []

    def on_after_batch_transfer(
        self,
        batch: TernarySample,
        batch_idx: int
    ) -> Tensor:
        for transform in self.batch_transforms or []:
            transform(batch, batch_idx)
        return batch

    def step(
        self,
        batch: TernarySample,
        batch_idx: int,
        stage: Stage,
        *args: Any,
        **kwargs: Any,
    ) -> Dict[str, Tensor]:
        step_outputs = self.model(*batch, *args, **kwargs)
        loss_outputs = self.loss(**step_outputs)
        self.log_dict(prefix_keys(loss_outputs, stage.name), batch_size=batch.x.size(0), prog_bar=True)
        step_outputs = { k: v.detach() for k, v in step_outputs.items() if isinstance(v, Tensor) }
        return loss_outputs, step_outputs

    @torch.no_grad()
    def inference_step(
        self,
        batch: TernarySample,
        batch_idx: int,
        *args: Any,
        **kwargs: Any,
    ) -> Dict[str, Tensor]:
        step_outputs = self.model(*batch, *args, **kwargs)
        step_outputs = { k: v.detach() for k, v in step_outputs.items() if isinstance(v, Tensor) }
        return step_outputs

    def training_step(
        self,
        batch: TernarySample,
        batch_idx: int
    ) -> Dict[str, Tensor]:
        loss_outputs, step_outputs = self.step(batch, batch_idx, Stage.train)
        self.training_step_outputs.append(step_outputs)
        return loss_outputs

    def on_train_epoch_end(self, step_outputs: List[Dict[str, Tensor]]) -> None:
        if hasattr(self.model, 'update') and callable(self.model.update):
            self.model.update()

    @torch.no_grad()
    def validation_step(
        self,
        batch: TernarySample,
        batch_idx: int
    ) -> Dict[str, Tensor]:
        loss_outputs, step_outputs = self.step(batch, batch_idx, Stage.val)
        self.validation_step_outputs.append(step_outputs)
        return loss_outputs

    @torch.no_grad()
    def test_step(
        self,
        batch: TernarySample,
        batch_idx: int,
        dataloader_idx: Optional[int] = None,
        *args: Any,
        **kwargs: Any,
    ) -> Dict[str, Tensor]:
        return self.inference_step(batch, batch_idx, *args, **kwargs)

    @torch.no_grad()
    def predict_step(
        self,
        batch: TernarySample,
        batch_idx: int,
        dataloader_idx: Optional[int] = None,
        *args: Any,
        **kwargs: Any,
    ) -> Tuple[Tensor, Tensor, Dict[str, Tensor]]:
        batch_output = self.inference_step(batch, batch_idx, *args, **kwargs)
        return batch.s, torch.tensor(dataloader_idx).expand(batch.x.size(0)), batch_output

    def configure_optimizers(self) -> Optimizer:
        optimiser_config = DictConfig({
            "_target_": self.optimiser_cls,
            **(self.optimiser_config or {})
        })
        optimiser = instantiate(optimiser_config, params=self.parameters(), lr=self.learning_rate)
        if self.scheduler_cls is not None:
            scheduler_config = DictConfig({
                "_target_": self.scheduler_cls,
                **(self.scheduler_config or {})
            })
            scheduler = instantiate(scheduler_config, optimizer=optimiser)
            pl_scheduler_config = {
                "scheduler": scheduler,
                "interval": self.scheduler_interval,
                "frequency": self.scheduler_frequency
            }
            return [optimiser], [pl_scheduler_config]
        return optimiser
