import torch
import pandas as pd

from conduit.data.structures import TernarySample
from dataclasses import dataclass
from einops.layers.torch import Rearrange
from itertools import chain
from pandas import DataFrame
from lightning import LightningModule
from torch import Tensor
from torch.nn import Module
from typing import (
    Any,
    Callable,
    Dict,
    List,
    Optional,
)

from soundscape_vae.algorithm.base import Algorithm
from soundscape_vae.types import Stage

__all__ = ["DatasetEncoder"]


@dataclass(unsafe_hash=True)
class DatasetEncoder(Algorithm):
    def __new__(cls, *args: Any, **kwargs: Any):
        obj = object.__new__(cls)
        LightningModule.__init__(obj)
        Algorithm.__init__(obj, *args, **kwargs)
        return obj

    @torch.no_grad()
    def step(
        self,
        batch: TernarySample,
        batch_idx: int,
        stage: Stage,
        *args: Any,
        **kwargs: Any,
    ) -> Dict[str, Tensor]:
        step_outputs = self.model(*batch)
        return dict(idx=batch.s.cpu()) | detach_values(step_outputs)

    def inference_step(self, *args, **kwargs):
        return self.step(*args, **kwargs)

    @torch.no_grad()
    def on_predict_step_end(
        self,
        batch_output: List[List[Dict[str, Tensor]]],
        batch: TernarySample,
        batch_idx: int,
        dataloader_idx: int,
    ) -> DataFrame:
        """aggregate batch embeddings into dataframe"""
        index = batch_output["idx"].tolist()
        embeddings = batch_output["logits"]
        file_idx = torch.tensor(dataloader_idx).expand(embeddings.size(0), 1)
        data = torch.hstack([file_idx, embeddings])
        embedding_columns = ['max', 'min', 'mean', 'std']
        columns = list(chain(["train/val/test"], embedding_columns))
        data = DataFrame(index=index, data=data, columns=columns)
        data.index.rename('file_i', inplace=True)
        return self.persistence_strategy.save(
            data,
            idx=f"_{dataloader_idx}_{batch_idx}"
        )

    @torch.no_grad()
    def on_predict_epoch_end(self, outputs: List[List[Dict[str, Tensor]]]) -> DataFrame:
        """aggregate batch data frames into single dataframe"""
        data = []
        for dataloader_idx, batch_output in enumerate(outputs):
           data.append(pd.concat([
               self.persistence_strategy.load(file_path)
               for batch_idx, file_path in enumerate(batch_output)
           ], axis=0))
        output = self.persistence_strategy.save(
            pd.concat(data, axis=0),
            run=self.logger.experiment
        )
        for batch_output in outputs:
            for file_path in batch_output:
                self.persistence_strategy.cleanup(file_path)
        return output

    def configure_optimizers(self):
        ...
