import torch

from dataclasses import dataclass
from einops.layers.torch import Rearrange
from torch.nn import Module
from torch import Tensor
from typing import Any
from soundscape_vae.models.base import Model

__all__ = ["FeatureEncoder"]

@dataclass(unsafe_hash=True)
class FeatureEncoder(Model):
    @torch.no_grad()
    def forward(self, x: Tensor, *args: Any, **kwargs: Any) -> Tensor:
        return dict(q_z=self.encoder(x))
