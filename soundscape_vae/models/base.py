import torch
from dataclasses import dataclass
from torch.nn import Module
from torch import Tensor
from typing import Any

__all__ = ["Model"]

@dataclass(unsafe_hash=True)
class Model(Module):
    encoder: Module
    decoder: Module

    def forward(self, x: Tensor, *args: Any, **kwargs: Any) -> Tensor:
        z = self.encoder(x)
        return self.decoder(z)
