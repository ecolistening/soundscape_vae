import torch

from dataclasses import dataclass
from torch.nn import Module
from torch import Tensor
from typing import (
    Dict,
    List,
    Tuple,
    Union,
)

__all__ = [
    "SummaryStats",
]

@dataclass
class SummaryStats(Module):
    dims: Union[int, List, Tuple] = (2, 3)

    def forward(self, x: Tensor) -> Tensor:
        return torch.hstack([
            x.amax(dim=self.dims),
            x.amin(dim=self.dims),
            x.mean(dim=self.dims),
            x.std(dim=self.dims),
        ])
