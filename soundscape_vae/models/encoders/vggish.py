import os
import copy
import torch

from dataclasses import dataclass
from torch import Tensor
from torch.nn import (
    Conv2d,
    init,
    Linear,
    MaxPool2d,
    Module, ReLU,
    Sequential,
)
from typing import (
    Any,
    Dict,
    List,
    Tuple,
    Optional,
)

from soundscape_vae.transforms import frame

__all__ = ["VGGish"]

@dataclass(unsafe_hash=True)
class VGGish(Module):
    pretrained: bool = True
    frozen: bool = False
    weight_init_std: float = 1e-2

    def __new__(cls, *args: Any, **kwargs: Any):
        obj = object.__new__(cls)
        Module.__init__(obj)
        return obj

    def __post_init__(self) -> None:
        self.features = self._make_layers()
        self.embeddings = self._make_embeddings()

        self.load_state_dict(torch.hub.load_state_dict_from_url(self.state_dict_url, progress=True))

        if self.frozen:
            for param in self.parameters():
                param.requires_grad_(False)
            print(f"Frozen parameters for {self.__class__.__name__}")

    @property
    def state_dict_url(self):
        return "https://github.com/harritaylor/torchvggish/releases/download/v0.1/vggish-10086976.pth"

    def forward(
        self,
        x: Tensor,
        *args: Any,
        **kwargs: Any,
    ) -> Dict[str, Tensor]:
        # shift time to dim 0 and chunk along time axis, then shift back
        x = x.permute(2, 0, 1, 3)
        x = frame(x, hop_length=96, window_length=96)
        x = x.permute(2, 0, 3, 1, 4)
        # extract for unflattening after forward pass
        bs, fr = x.size(0), x.size(1)
        # frames are stacked into a single batch
        x = x.flatten(0, 1).float()
        x = self.features(x)
        x = self.flatten(x)
        x = self.embeddings(x)
        # unstack batch from frames
        x = x.unflatten(0, (bs, fr))
        return dict(x=x)

    def flatten(self, x: Tensor) -> Tensor:
        return x.permute(0, 2, 3, 1).contiguous().view(x.size(0), -1)

    def _make_embeddings(self) -> Module:
        layers = []
        sizes = [512 * 6 * 4, 4096, 4096, 128]
        for i in range(len(sizes) - 1):
            layer = Linear(in_features=sizes[i], out_features=sizes[i + 1])
            init.trunc_normal_(layer.weight)
            init.zeros_(layer.bias)
            layers.append(layer)
            if i <= len(sizes) - 3:
                layers.append(ReLU(True))
        return Sequential(*layers)

    def _make_layers(self) -> Module:
        layers = []
        in_channels = 1
        for v in [64, "M", 128, "M", 256, 256, "M", 512, 512, "M"]:
            if v == "M":
                layers.append(MaxPool2d(kernel_size=2, stride=2))
            else:
                conv = Conv2d(in_channels, v, kernel_size=3, padding=1)
                init.trunc_normal_(conv.weight, std=self.weight_init_std)
                init.zeros_(conv.bias)
                layers.append(conv)
                layers.append(ReLU(inplace=True))
                in_channels = v
        return Sequential(*layers)
