from dataclasses import dataclass, field
import torch
from torch import Tensor
from torch.nn import (
    AvgPool2d,
    BatchNorm2d,
    Conv2d,
    Dropout,
    Identity,
    LeakyReLU,
    Linear,
    MaxPool2d,
    Module,
    ModuleList,
    ReLU,
    Sequential,
)
from typing import (
    Any,
    Callable,
    List,
    Tuple,
    Type,
    Optional,
)
from soundscape_vae.utils import Concat

__all__ = ["ResNet"]

@dataclass(unsafe_hash=True)
class ResNet(Module):
    width: int
    depth: int
    block_sizes: List[int] = field(default_factory=lambda: [8, 16, 32, 64, 128, 256])
    dropout_prob: float = 0.5
    padding_mode: str = "zeros"

    def __post_init__(self) -> None:
        self.blocks = ModuleList()
        block_kwargs = dict(dropout_prob=dropout_prob, padding_mode=padding_mode)
        in_channels = self.block_sizes[0] * self.width
        for block_size in self.block_sizes[1:]:
            out_channels = block_size * self.width
            self.blocks.append(Sequential(
                ResidualBottleneck(in_channels, out_channels, **block_kwargs),
                *(ResidualBlock(out_channels, out_channels, **block_kwargs) for i in range(self.depth - 1))
            ))
            in_channels = out_channels

    def forward(self, x: Tensor) -> Tuple[Tensor, List[Tensor]]:
        for block in self.blocks:
            x = block(x)
        return x


class ResidualBlock(Module):
    def __init__(
        self,
        in_channels: int,
        out_channels: int,
        dropout_prob: float = 0.5,
        padding_mode: str = 'zeros',
    ) -> None:
        super().__init__()
        self.block = Sequential(
            Conv2d(in_channels, out_channels, kernel_size=3, padding=1, bias=False, padding_mode=padding_mode),
            BatchNorm2d(out_channels),
            ReLU(),
            Dropout(dropout_prob),
            Conv2d(out_channels, out_channels, kernel_size=3, padding=1, bias=False, padding_mode=padding_mode),
        )
        self.shortcut = Identity() if in_channels == out_channels else Sequential(
            Conv2d(in_channels, out_channels, kernel_size=1, bias=False),
            BatchNorm2d(num_features=out_channels),
        )

    def forward(self, x: Tensor) -> Tensor:
        return self.block(x) + self.shortcut(x)


class ResidualBottleneck(Module):
    def __init__(
        self,
        in_channels: int,
        out_channels: int,
        dropout_prob: float = 0.5,
        padding_mode: str = 'zeros',
    ) -> None:
        super().__init__()
        self.block = Sequential(
            Conv2d(in_channels, in_channels, kernel_size=1, bias=False),
            BatchNorm2d(in_channels),
            ReLU(),
            Conv2d(in_channels, in_channels, kernel_size=3, padding=1, bias=False, padding_mode=padding_mode),
            BatchNorm2d(in_channels),
            ReLU(),
            Conv2d(in_channels, in_channels * 2, kernel_size=2, stride=2, bias=False, padding_mode=padding_mode),
            Conv2d(in_channels * 2, in_channels, kernel_size=1, bias=True),
            Dropout(dropout_prob),
            Conv2d(in_channels, out_channels, kernel_size=1, bias=False),
        )
        self.shortcut = Sequential(
            Conv2d(in_channels, in_channels * 2, kernel_size=2, stride=2, bias=False, padding_mode=padding_mode),
            Conv2d(in_channels * 2, in_channels, kernel_size=1, bias=True),
            Conv2d(in_channels, out_channels, kernel_size=1, bias=False),
        )

    def forward(self, x: Tensor) -> Tensor:
        return self.block(x) + self.shortcut(x)
