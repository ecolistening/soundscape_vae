import torch

from dataclasses import dataclass
from torch import Tensor
from typing import (
    Any,
    Callable,
    Dict,
    List,
    Tuple,
    Optional,
)
from torch.nn import (
    BatchNorm2d,
    Conv2d,
    Dropout,
    Identity,
    init,
    Linear,
    Module,
    ModuleList,
    ReLU,
    Sequential,
)

__all__ = ["BirdNet"]

@dataclass(unsafe_hash=True)
class BirdNet(Module):
    width: int = 4
    depth: int = 3
    dropout_prob: float = 0.5
    weight_init_std: float = 1e-2
    frozen: bool = False

    def __new__(cls, *args: Any, **kwargs: Any):
        obj = object.__new__(cls)
        Module.__init__(obj)
        return obj

    @property
    def out_channels(self) -> int:
        return self.block_sizes[-1] * self.width

    def __post_init__(self) -> None:
        self.block_sizes = [8, 16, 32, 64, 128]
        out_channels = self.block_sizes[0] * self.width
        # pre-processing block scales input and downsamples in time
        self.pre_process = Sequential(
            Conv2d(1, out_channels, kernel_size=5, padding=2, padding_mode="replicate"),
            BatchNorm2d(num_features=out_channels),
            ReLU(),
            Conv2d(out_channels, out_channels * 2, kernel_size=(2, 1), stride=(2, 1), padding_mode="replicate"),
            Conv2d(out_channels * 2, out_channels, kernel_size=1),
        )
        init.trunc_normal_(self.pre_process[0].weight, std=self.weight_init_std)
        # main CNN is a wide residual network
        self.features = ModuleList()
        in_channels = out_channels
        block_kwargs = dict(dropout_prob=self.dropout_prob)
        for block_size in self.block_sizes[1:]:
            out_channels = block_size * self.width
            self.features.append(Sequential(
                ResidualBottleneck(in_channels, out_channels, **block_kwargs),
                *(ResidualBlock(out_channels, out_channels, **block_kwargs) for i in range(self.depth - 1))
            ))
            in_channels = out_channels
        # fix parameters if specified
        if self.frozen:
            for param in self.parameters():
                param.requires_grad_(False)
            print(f"Frozen parameters for {self.__class__.__name__}")

    def forward(self, x: Tensor) -> Tuple[Tensor, List[Tensor]]:
        x = self.pre_process(x)
        for block in self.features:
            x = block(x)
        return x


class ResidualBlock(Module):
    def __init__(
        self,
        in_channels: int,
        out_channels: int,
        dropout_prob: float = 0.5,
    ) -> None:
        super().__init__()
        self.block = Sequential(
            Conv2d(in_channels, out_channels, kernel_size=3, padding=1, bias=False, padding_mode="replicate"),
            BatchNorm2d(num_features=out_channels),
            ReLU(),
            Dropout(dropout_prob),
            Conv2d(out_channels, out_channels, kernel_size=3, padding=1, bias=False, padding_mode="replicate"),
        )
        self.shortcut = Identity() if in_channels == out_channels else Sequential(
            Conv2d(in_channels, out_channels, kernel_size=1, bias=False),
            BatchNorm2d(num_features=out_channels),
        )

    def forward(self, x: Tensor) -> Tensor:
        return self.block(x) + self.shortcut(x)


class ResidualBottleneck(Module):
    def __init__(
        self,
        in_channels: int,
        out_channels: int,
        dropout_prob: float = 0.5,
    ) -> None:
        super().__init__()
        self.block = Sequential(
            Conv2d(in_channels, in_channels, kernel_size=1, bias=False),
            BatchNorm2d(in_channels),
            ReLU(),
            Conv2d(in_channels, in_channels, kernel_size=3, padding=1, bias=False, padding_mode="replicate"),
            BatchNorm2d(in_channels),
            ReLU(),
            Conv2d(in_channels, in_channels * 2, kernel_size=2, stride=2, bias=False, padding_mode="replicate"),
            Conv2d(in_channels * 2, in_channels, kernel_size=1, bias=True),
            Dropout(dropout_prob),
            Conv2d(in_channels, out_channels, kernel_size=1, bias=False),
        )
        self.shortcut = Sequential(
            Conv2d(in_channels, in_channels * 2, kernel_size=2, stride=2, bias=False, padding_mode="replicate"),
            Conv2d(in_channels * 2, in_channels, kernel_size=1, bias=True),
            Conv2d(in_channels, out_channels, kernel_size=1, bias=False),
        )

    def forward(self, x: Tensor) -> Tensor:
        return self.block(x) + self.shortcut(x)
