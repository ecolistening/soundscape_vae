import einops
import torch

from dataclasses import dataclass
from torch import Tensor
from torch.nn import Module, Linear, Parameter
from torch.functional import F
from torch.distributions.normal import Normal
from typing import (
    Any,
    Dict,
    Callable,
    List,
    Optional,
    Tuple,
    Type,
    TypeVar,
    Union,
)
from soundscape_vae import transforms

__all__ = ["VAE"]

@dataclass(unsafe_hash=True)
class VAE(Module):
    """
    A VAE for spectrogram audio. Applies an encoder before chunking in time and passing
    through a bottleneck to output a posterior distribution for each time window.

    The standard VAE is composed of a neural network encoder parameterised by weights ϕ
    and a neural network decoder parameterised by weights θ, and is trained to maximise
    a lower bound on the model (ELBO), i.e. an approximation to the marginal distribution
    in Bayes rule. The loss is formulated as:

      L(θ,ϕ,x_i) = -D_KL(q_ϕ(z|x_i)||p_θ(z)) + E_z~q_ϕ[log p_θ(x_i|z)]

    The ELBO minimises the KL divergence between the surrogate distribution and the prior
    on z while maximising the likelihood p(x_i|z_i). A simplifying assumption is made that
    our probability distributions are gaussian so we can analytically determine the D_KL
    between the prior and the surrogate posterior

      D_KL(q_ϕ(z|x_i)||p_θ(z)) = 1/2 * sum(1 + log(sigma^2) - mu^2 - sigma^2)

    By maximising the likelihood of the data derived from the learned latent distribution
    with samples z_i, the model learns to reconstruct the input data x_i to output a variational
    approximation x_hat_i while learning an approximation q_ϕ(z|x_i) to the true posterior which
    produces a mapping that increases the accuracy of the reconstruction over all x_i

    :param Module encoder: A pytorch encoder that outputs a 2D representation of a data point
    :param Module decoder: A pytorch decoder that receives a 2D representation of a data point
    :param int window_length: The size of the receptive field of temporal framing over the CNN encoded 2D representation
    :param int hop_length: The size of the temporal frame over the CNN encoded 2D representation
    :param int frequency_resolution: The size of the CNN encoded 2D representation along the frequency axis
    :param int latent_dim: The dimensionality of the latent distribution for each time window
    """
    encoder: Module
    decoder: Module
    window_length: int = 6
    hop_length: int = 6
    frequency_resolution: int = 4
    latent_dim: int = 128

    def __new__(cls, *args: Any, **kwargs: Any):
        obj = object.__new__(cls)
        Module.__init__(obj)
        return obj

    def __post_init__(self):
        self.encoder_bottleneck = Linear(
            in_features=self.encoder.out_channels * self.hop_length * self.frequency_resolution,
            out_features=2 * self.latent_dim,
        )
        self.decoder_bottleneck = Linear(
            in_features=self.latent_dim,
            out_features=self.encoder.out_channels * self.hop_length * self.frequency_resolution,
        )
        self.log_sigma_z = Parameter(torch.tensor(1.0).log(), requires_grad=True)

    def forward(self, x: Tensor, *args: Any, **kwargs: Any) -> Dict[str, Tensor]:
        x = x.float()
        q_z = self.encode(x)
        z = self.reparameterise(q_z)
        mu_z = self.decode(z)
        log_sigma_z = -6 + F.softplus(self.log_sigma_z + 6)
        return dict(x=x, mu_z=mu_z, log_sigma_z=self.log_sigma_z, q_z=q_z, z=z)

    def encode(self, x: Tensor) -> Tensor:
        # encode using CNN
        x = self.encoder(x)
        # shift time to outer axis and apply temporal framing
        x = einops.rearrange(x, "bs ch ts fq -> ts bs fq ch")
        x = transforms.frame(x, window_length=self.window_length, hop_length=self.hop_length)
        # shift time back to inner axis and keep frames on outer dimension
        x = einops.rearrange(x, "fr ts bs fq ch -> bs fr (ts fq ch)")
        # linear projection of each frame
        x = self.encoder_bottleneck(x)
        return x

    def decode(self, x: Tensor) -> Tensor:
        # linear projection of each frame
        x = self.decoder_bottleneck(x)
        # shift time and frames to outer axes
        x = einops.rearrange(
            x, "bs fr (ts fq ch) -> fr ts bs fq ch",
            ch=self.encoder.out_channels,
            ts=self.hop_length,
            fq=self.frequency_resolution
        )
        # reverse temporal framing
        x = transforms.unframe(x, window_length=self.window_length, hop_length=self.hop_length)
        # shift time back to inner axis
        x = einops.rearrange(x, "ts bs fq ch -> bs ch ts fq")
        # decode using CNN
        x = self.decoder(x)
        return x

    def reparameterise(self, q_z: Tensor) -> Tensor:
        mu, log_sigma_sq = q_z.chunk(2, dim=-1)
        # soft limit the variance to 6
        log_sigma_sq = -6 + F.softplus(log_sigma_sq + 6)
        sigma = (1/2 * log_sigma_sq).exp()
        return Normal(mu, sigma).rsample()
