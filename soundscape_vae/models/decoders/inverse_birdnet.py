from dataclasses import dataclass, field
from einops.layers.torch import Rearrange
from torch import Tensor
from torch.nn import (
    BatchNorm2d,
    Conv2d,
    ConvTranspose2d,
    Dropout,
    GELU,
    Identity,
    LeakyReLU,
    Linear,
    Module,
    ModuleList,
    ReLU,
    Sequential,
    Upsample,
)
from typing import (
    Any,
    Dict,
    List,
    Tuple,
    Optional,
)

__all__ = ["InverseBirdNet"]

@dataclass(unsafe_hash=True)
class InverseBirdNet(Module):
    width: int = 4
    depth: int = 3
    dropout_prob: float = 0.5
    frozen: bool = False

    def __new__(cls, *args: Any, **kwargs: Any):
        obj = object.__new__(cls)
        Module.__init__(obj)
        return obj

    def __post_init__(self) -> None:
        block_sizes = [128, 64, 32, 16, 8]
        # main CNN is a wide residual network
        self.features = ModuleList()
        in_channels = block_sizes[0] * self.width
        block_kwargs = dict(dropout_prob=self.dropout_prob)
        for block_size in block_sizes[1:]:
            out_channels = block_size * self.width
            self.features.append(Sequential(
                ResidualBottleneck(in_channels, out_channels, **block_kwargs),
                *(ResidualBlock(out_channels, out_channels, **block_kwargs) for i in range(self.depth - 1))
            ))
            in_channels = out_channels
        # post-processing block upsamples in time
        in_channels = self.width * block_sizes[-1]
        self.post_process = Sequential(
            Conv2d(in_channels, in_channels * 2, kernel_size=1),
            BatchNorm2d(num_features=in_channels * 2),
            ReLU(),
            ConvTranspose2d(in_channels * 2, in_channels, kernel_size=(2, 1), stride=(2, 1)),
            BatchNorm2d(num_features=in_channels),
            ReLU(),
            Conv2d(in_channels, 1, kernel_size=5, padding=2, padding_mode="replicate"),
        )
        if self.frozen:
            for param in self.parameters():
                param.requires_grad_(False)
            print(f"Frozen parameters for {self.__class__.__name__}")

    def forward(self, x: Tensor) -> Tensor:
        for block in self.features:
            x = block(x)
        x = self.post_process(x)
        return x


class ResidualBlock(Module):
    def __init__(
        self,
        in_channels: int,
        out_channels: int,
        dropout_prob: float = 0.5,
    ) -> None:
        super().__init__()
        self.block = Sequential(
            Conv2d(in_channels, out_channels, kernel_size=3, padding=1, bias=False, padding_mode="replicate"),
            BatchNorm2d(num_features=out_channels),
            ReLU(),
            Dropout(dropout_prob),
            Conv2d(out_channels, out_channels, kernel_size=3, padding=1, bias=False, padding_mode="replicate"),
        )
        self.shortcut = Identity() if in_channels == out_channels else Sequential(
            Conv2d(in_channels, out_channels, kernel_size=1, bias=False),
            BatchNorm2d(num_features=out_channels),
        )

    def forward(self, x: Tensor) -> Tensor:
        return self.block(x) + self.shortcut(x)


class ResidualBottleneck(Module):
    def __init__(
        self,
        in_channels: int,
        out_channels: int,
        dropout_prob: float = 0.5,
    ) -> None:
        super().__init__()
        self.block = Sequential(
            Conv2d(in_channels, in_channels, kernel_size=1, bias=False),
            BatchNorm2d(in_channels),
            ReLU(),
            Conv2d(in_channels, in_channels, kernel_size=3, padding=1, bias=False, padding_mode="replicate"),
            BatchNorm2d(in_channels),
            ReLU(),
            Sequential(
                ConvTranspose2d(in_channels, in_channels // 2, kernel_size=2, stride=2),
                BatchNorm2d(in_channels // 2),
                ReLU(),
            ),
            Conv2d(in_channels // 2, in_channels, kernel_size=1, bias=True),
            Dropout(dropout_prob),
            Conv2d(in_channels, out_channels, kernel_size=1, bias=False),
        )
        self.shortcut = Sequential(
            Sequential(
                ConvTranspose2d(in_channels, in_channels // 2, kernel_size=2, stride=2),
                BatchNorm2d(in_channels // 2),
                ReLU(),
            ),
            Conv2d(in_channels // 2, in_channels, kernel_size=1, bias=True),
            Conv2d(in_channels, out_channels, kernel_size=1, bias=False),
        )

    def forward(self, x: Tensor) -> Tensor:
        return self.block(x) + self.shortcut(x)

