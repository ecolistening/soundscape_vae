import colorcet as cc
import einops
import librosa
import matplotlib as mpl
import numpy as np
import pandas as pd
import seaborn as sns
import torch

from itertools import combinations
from librosa import display as libd
from matplotlib.colors import TwoSlopeNorm
from matplotlib import colors as mcolors
from matplotlib import pyplot as plt
from matplotlib import ticker
from matplotlib.axes import Axes
from matplotlib.collections import QuadMesh
from matplotlib.colors import Colormap, Normalize
from matplotlib.figure import Figure
from matplotlib.patches import Patch
from numpy.typing import NDArray
from pandas import DataFrame
from sklearn import metrics
from torch import Tensor
from torch.distributions import Normal
from typing import (
    Any,
    Callable,
    Dict,
    List,
    Optional,
    Tuple,
    Union,
)

from soundscape_vae.utils import interpolate

def plot_spectrogram_residual(
    X_diff,
    ax: Optional[Axes] = None,
    cbar: bool = True,
    norm: bool = True,
    vmin: Optional[float] = None,
    vmax: Optional[float] = None,
    **kwargs: Any
) -> Tuple[Figure, Axes, QuadMesh]:
    fig, ax = plt.subplots() if not ax else (plt.gcf(), ax)
    # compute the lower and upper bound for the z-axis if none provided
    if not vmin and not vmax:
        # ensure z-axis normalisation remains linear
        bound = max(abs(X_diff).max(), abs(X_diff).min())
        vmin, vmax = -bound, bound
    # normalise z-axis to ensure divergent colormaps centered around 0
    norm = TwoSlopeNorm(
        vmin=vmin,
        vcenter=0.0,
        vmax=vmax
    ) if norm else None
    # render spectrogram
    mesh = libd.specshow(
        data=X_diff,
        ax=ax,
        norm=norm,
        **kwargs,
    )
    # render colorbar if specified
    if cbar:
        plt.colorbar(
            mesh,
            ax=ax,
            orientation="vertical",
            format="%+3.1f dB"
        )
    return fig, ax, mesh


def plot_confusion_matrix(
    y_true: Union[Tensor, NDArray],
    y_pred: Union[Tensor, NDArray],
    classes: Union[List, NDArray],
    cmap: Colormap = sns.color_palette("rocket", as_cmap=True),
    ax: Optional[Axes] = None,
    figsize: Optional[Tuple[int, int]] = (8, 6),
    **kwargs: Any,
) -> Tuple[Figure, Axes]:
    fig, ax = (plt.gcf, ax) if ax is not None else plt.subplots(figsize=figsize)
    cm = metrics.confusion_matrix(y_true, y_pred, **kwargs)
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    cm = pd.DataFrame(cm, classes, classes)
    sns.heatmap(cm, annot=True, fmt='.2f', cmap=cmap, ax=ax)
    ax.set_ylabel('Actual')
    ax.set_xlabel('Predicted')
    ax.set_title(f"Accuracy: {metrics.accuracy_score(y_true, y_pred).round(3)}")
    return fig, ax


def plot_feature_importance_directional(
    X: Union[Tensor, NDArray],
    W: Union[Tensor, NDArray],
    classes: Union[List, NDArray],
    figsize: Tuple[int, int] = (15, 2),
    cmap: Colormap = 'PuOr',
) -> Tuple[Figure, Axes]:
    fig, ax = plt.subplots(figsize=figsize)
    feature_importance = X.std(axis=0) * W
    bound = max(abs(feature_importance).max(), abs(feature_importance).min())
    norm = TwoSlopeNorm(vmin=-bound, vcenter=0.0, vmax=bound)
    sns.heatmap(feature_importance, cmap=cmap, yticklabels=classes, norm=norm, ax=ax)
    ax.tick_params(axis='x', rotation=45)
    ax.tick_params(axis='y', rotation=0)
    for label in ax.get_xticklabels():
        label.set_fontsize(8)
    return fig, ax


def plot_pairwise_feature_importance_directional(
    X: Union[Tensor, NDArray],
    W: Union[Tensor, NDArray],
    class_pairs: Union[List[str], NDArray],
) -> Tuple[Figure, Axes]:
    ys = list(combinations(classes, 2))

    xs = np.empty((len(ys), 128))
    for i, model in enumerate(W):
        pair_feature_importance = X_test.std(axis=0) * model.coef_
        xs[i] = pair_feature_importance.flatten()

    fig, ax = plt.subplots(figsize=(15, 8))
    bound = max(abs(xs).max(), abs(xs).min())
    norm = TwoSlopeNorm(vmin=-bound, vcenter=0.0, vmax=bound)
    sns.heatmap(xs, cmap='PuOr', yticklabels=[f"{classes[i]} {classes[j]}" for i, j in ys], norm=norm, ax=ax)
    ax.tick_params(axis='x', rotation=45)
    ax.tick_params(axis='y', rotation=0)
    for label in ax.get_xticklabels():
        label.set_fontsize(8)
    ax.set_title('Feature Importance for One vs One (Binary) Logistic Regression between Pairwise Habitats \n')
    return fig, ax


def plot_latent_histogram_feature_importance_2d(
    feature_importances: DataFrame,
    figsize: Tuple[int, int] = (10, 3),
    cmap: Colormap = sns.color_palette("icefire", as_cmap=True),
) -> Tuple[Figure, Axes]:
    classes, bins = feature_importances.axes
    num_classes, num_bins = len(classes), len(bins)
    extent = [min(bins), max(bins), 0, num_classes]
    bound = max(abs(max(feature_importances)), abs(min(feature_importances)))
    norm = TwoSlopeNorm(vcenter=0.0, vmax=bound, vmin=-bound)
    fig, ax = plt.subplots(figsize=figsize)
    im = ax.imshow(feature_importances, cmap=cmap, extent=extent, aspect=0.5)
    cax = fig.add_axes([
        ax.get_position().x1 + 0.01,
        ax.get_position().y0,
        0.02,
        ax.get_position().height,
    ])
    cbar = fig.colorbar(im, cax=cax, orientation="vertical")
    ax.set_xticks(np.linspace(-5, 5, num_bins + 1).round(1))
    ax.set_yticks(np.arange(6) + 0.5, list(reversed(classes)))
    ax.tick_params(axis='x', rotation=45)
    ax.set_xlabel("Feature Range")
    ax.set_ylabel("Habitat")
    return fig, ax


def plot_latent_histogram(
    X: Union[Tensor, NDArray],
    z_min: float,
    z_max: float,
    figsize: Optional[Tuple[int, int]] = (7, 12),
    cmap: Colormap = sns.color_palette("mako", as_cmap=True),
    norm: Optional[Normalize] = None,
    ax: Optional[Axes] = None,
) -> Tuple[Figure, Axes]:
    fig, ax = plt.subplots(figsize=figsize) if ax is None else (plt.gcf(), ax)
    num_bins, latent_dim = X.shape
    xx, yy = np.meshgrid(np.linspace(z_min, z_max, num_bins), np.arange(latent_dim))
    mesh = ax.pcolormesh(xx, yy, X.T, cmap=cmap, shading='auto', norm=norm)
    cbar = fig.colorbar(mesh, ax=ax, orientation='vertical', pad=0.1)
    cbar.set_label('P(z)', va="bottom", rotation=0)
    ax.set_xticks(np.linspace(z_min, z_max, num_bins + 1))
    ax.tick_params(axis='x', rotation=45)
    ax.tick_params(axis='y', rotation=0)
    return fig, ax, mesh, cbar


def ax_to_mel_scale(ax: Axes) -> Axes:
    ax.set_xscale("symlog", linthresh=1000.0, base=2)
    ax.xaxis.set_major_formatter(ScalarFormatter())
    ax.xaxis.set_major_locator(SymmetricalLogLocator(ax.xaxis.get_transform()))
    return ax


def make_ax_invisible(ax: Axes):
    # set background to white
    ax.set_facecolor('white')
    # make spines invisible
    [ax.spines[side].set_visible(False) for side in ax.spines]
    # set grid to white
    ax.grid(which='major', color='white', linewidth=1.2)
    ax.grid(which='minor', color='white', linewidth=0.6)
    ax.minorticks_on()
    ax.tick_params(which='minor', bottom=False, left=False)
    ax.xaxis.set_minor_locator(ticker.AutoMinorLocator(2))
    ax.yaxis.set_minor_locator(ticker.AutoMinorLocator(2))
    # remove all ticks
    ax.set_yticks([])
    ax.set_xticks([])


def plot_feature_importance(
    data: NDArray,
    y_tick_labels: List[str] = [],
    x_tick_labels: List[str] = [],
    cmap: Colormap = cc.linear_wcmr_100_45_c42, #sns.color_palette("mako_r", as_cmap=True),
    figsize: Tuple[int, int] = (15, 3),
    orientation: str = 'horizontal',
    ax: Axes = None,
    **kwargs,
) -> Tuple[Figure, Axes]:
    fig, ax = plt.subplots(figsize=figsize) if not ax else (plt.gcf(), ax)
    if orientation == 'vertical':
        data = data.T
        x_tick_labels, y_tick_labels = y_tick_labels, x_tick_labels

    res = sns.heatmap(
        data,
        cmap=cmap,
        xticklabels=x_tick_labels if len(x_tick_labels) else list(range(data.shape[1])),
        yticklabels=y_tick_labels if len(y_tick_labels) else list(range(data.shape[0])),
        ax=ax,
        **kwargs,
    )

    if orientation == 'vertical':
        ax.yaxis.tick_right()
        ax.tick_params(axis='x', rotation=90)
        ax.tick_params(axis='y', rotation=0)
        for label in ax.get_yticklabels():
            label.set_fontsize(6)
        for label in ax.get_xticklabels():
            label.set_fontsize(6)
    else:
        ax.tick_params(axis='x', rotation=90)
        ax.tick_params(axis='y', rotation=0)
        for label in ax.get_xticklabels():
            label.set_fontsize(8)

    ax.patch.set_edgecolor('black')
    ax.patch.set_linewidth(1)
    return fig, ax
