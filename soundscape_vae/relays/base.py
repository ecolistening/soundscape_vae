import datetime as dt
import torch
import wandb
import lightning

from dataclasses import dataclass
from tempfile import TemporaryDirectory
from torchvision.transforms import Compose
from conduit.data.datamodules import CdtDataModule
from hydra.utils import instantiate
from omegaconf import (
    OmegaConf,
    DictConfig,
)
from pathlib import Path
from pprint import pprint as pp
from lightning import (
    Callback,
    LightningModule,
    Trainer,
)
from lightning.pytorch.loggers import Logger
from ranzen.hydra import Option, Relay
from torch.nn import Module
from typing import (
    Any,
    Dict,
    List,
    Optional,
    Union,
)

from soundscape_vae.algorithm import Algorithm
from soundscape_vae.callbacks import ModelCheckpoint
from soundscape_vae.utils import to_snake_case
from soundscape_vae.types import Stage

@dataclass(kw_only=True)
class EcolisteningRelay(Relay):
    algorithm: DictConfig
    callback: DictConfig
    checkpoint: DictConfig
    data_module: DictConfig
    logger: DictConfig
    model: DictConfig
    trainer: DictConfig
    transforms: DictConfig

    seed: Optional[int] = 42
    artefact_id: Optional[str] = None
    save_dir: Optional[str] = str(Path.home() / "models")

    @classmethod
    def with_hydra(
        cls,
        root: Union[Path, str],
        *,
        clear_cache: bool = False,
        algorithm: List[Union[Any, Option]],
        callback: List[Union[Any, Option]],
        data_module: List[Union[Any, Option]],
        logger: List[Union[Any, Option]],
        model: List[Union[Any, Option]],
        transforms: List[Union[Any, Option]],
    ) -> None:
        configs = dict(
            algorithm=algorithm,
            callback=callback,
            checkpoint=[Option(class_=ModelCheckpoint, name="base")],
            data_module=data_module,
            model=model,
            logger=logger,
            trainer=[Option(class_=Trainer, name="base")],
            transforms=transforms,
        )
        super().with_hydra(
            root=root,
            clear_cache=clear_cache,
            instantiate_recursively=False,
            **configs,
        )

    def run(self, config: Optional[Dict[str, Any]] = None) -> None:
        pp(config, width=100)
        lightning.seed_everything(self.seed, workers=True)

        # TODO: figure out why this isn't working properly
        # transforms = instantiate(self.transforms)
        transforms = []
        for transform_config in self.transforms["transforms"]:
            transforms.append(instantiate(transform_config))
        transforms = Compose(transforms)

        data_module: CdtDataModule = instantiate(self.data_module)
        if hasattr(data_module, "train_transforms"):
            data_module.train_transforms = transforms
            data_module.test_transforms = transforms
        data_module.prepare_data()
        data_module.setup()

        if self.logger.get("group", None) is None:
            default_group = f"{data_module.__class__.__name__}"
            self.logger["group"] = default_group
        logger: Logger = instantiate(self.logger, reinit=True)
        logger.log_hyperparams(config)

        if self.artefact_id is not None:
            algorithm = Algorithm.from_artefact(artefact_id=self.artefact_id, save_dir=self.save_dir)
        else:
            algorithm: Algorithm = instantiate(self.algorithm, model=self.model)

        trainer: Trainer = instantiate(self.trainer, logger=logger)
        trainer.callbacks.append(instantiate(self.callback))
        checkpoint = instantiate(self.checkpoint)
        trainer.callbacks.append(checkpoint)

        trainer.fit(
            algorithm,
            train_dataloaders=data_module.train_dataloader(),
            val_dataloaders=data_module.val_dataloader()
        )

        if trainer.max_steps > 0:
            with TemporaryDirectory() as tmp_dir:
                tmp_dir = Path(tmp_dir)
                model_save_path = tmp_dir / "model.pt"
                torch.save(dict(state_dict=algorithm.state_dict(), config=config), f=model_save_path)
                model_name = (f"{to_snake_case(algorithm.model.__class__.__name__)}_"
                              f"{to_snake_case(algorithm.model.encoder.__class__.__name__)}.pt")
                artefact = wandb.Artifact(model_name, type="model", metadata=config)
                artefact.add_file(str(model_save_path.resolve()), name="model.pt")
                logger.experiment.log_artifact(artefact)
                artefact.wait()
                print(f"Artifact ID: {logger.experiment.entity}/{logger.experiment.project}/{artefact.name}")

        trainer.predict(algorithm, dataloaders=data_module.predict_dataloader())
