import re
import copy
import pandas as pd
import numpy as np
import torch

from librosa import display as libd
from pandas import DataFrame
from matplotlib import pyplot as plt
from matplotlib.ticker import ScalarFormatter, SymmetricalLogLocator
from matplotlib.axes import Axes
from numpy.typing import NDArray
from omegaconf import OmegaConf
from pandas import DataFrame
from sklearn.neighbors import KernelDensity
from torch import Tensor
from torch.nn import Module, ModuleList
from torch.distributions import Normal
from typing import (
    Any,
    Callable,
    Dict,
    List,
    Optional,
    Tuple,
    TypeVar,
    Union,
)

T = TypeVar("T")

class PrintShape(Module):
    def __init__(self, label: Union[str, int] = ''):
        super().__init__()
        self.label = label

    def forward(self, x):
        print(self.label, x.shape)
        return x


class Concat(ModuleList):
    def __init__(self, *args, dim: int = 1, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.dim = dim

    def forward(self, x: Tensor) -> Tensor:
        return torch.cat(tuple(mod(x) for mod in self), dim=self.dim)


class LogMeanExponential(Module):
    def __init__(self, dim=None, keepdim=False, sharpness=5.0):
        self.dim = [] if dim is None else dim
        self.keepdim = keepdim
        self.sharpness = sharpness

    def forward(
        self,
        x: Tensor
    ) -> Tensor:
        x_max = x.max(dim=self.dim, keepdim=True)
        x_max_2 = x.max(dim=self.dim, keepdim=self.keepdim)
        x = self.sharpness * (x - x_max)
        x = ((x.exp()).mean(dim=self.dim, keepdim=self.keepdim)).log()
        return x / self.sharpness + x_max_2

def register_resolvers():
    OmegaConf.register_new_resolver("sum", lambda *array: sum(array))
    OmegaConf.register_new_resolver("prod", lambda *array: np.product(array))
    OmegaConf.register_new_resolver("div", lambda *array: np.divide(array))
    OmegaConf.register_new_resolver("divmod", lambda *array: np.product(array))
    OmegaConf.register_new_resolver("count", lambda *array: len(array))

def required() -> T:
    def error() -> T:
        field_name = f.name
        raise ValueError(f"field '{field_name}' required")

    return field(default_factory=error)

def default(attr: Optional[None], default):
    return attr if attr is not None else default

def unity(*args: Any, **kwargs: Any) -> float:
    return 1.0

def noop(x: T) -> T:
    return x

def pluck(struct: Union[Dict, List], accessor: List[Any]):
    if isinstance(struct, dict):
        return {k: v for k, v in struct.items() if k in accessor}
    elif isinstance(struct, list):
        return [d.get(accessor) for d in struct]
    else:
        raise Exception(f"struct argument is invalid type f{type(struct)}")

def try_or(fn, default=None):
    try:
        return fn()
    except:
        return default

def cpu_values(_dict):
    return { k: v.cpu() for k, v in _dict.items() if isinstance(v, Tensor) }

def prefix_keys(d: Dict, prefix: str, separator: str = '/') -> Dict[str, Any]:
    return { f"{prefix}{separator}{key}": value for key, value in d.items() }

def detach_values(d: Dict[str, Tensor]) -> Dict[str, Tensor]:
    return { k: v.detach() for k, v in d.items() if isinstance(v, Tensor) }

def to_snake_case(word):
    word = re.sub(r"([A-Z]+)([A-Z][a-z])", r'\1_\2', word)
    word = re.sub(r"([a-z\d])([A-Z])", r'\1_\2', word)
    word = word.replace("-", "_")
    return word.lower()

# ----------------------------- Modelling --------------------------- #

def sampler(mu: Tensor, sigma: Tensor, num_samples: int = 100) -> Tensor:
    dist = Normal(mu, sigma)
    return torch.cat([dist.rsample() for i in range(num_samples)], dim=1)

def residual(
    decoder: Callable,
    X_start: Union[NDArray, Tensor],
    X_end: Union[NDArray, Tensor],
    scale_transform: Callable = lambda x: x
) -> Union[NDArray, Tensor]:
    # ensure tensor and decode each output
    with torch.no_grad():
        X_hat_start = decoder(torch.as_tensor(X_start).float())
        X_hat_end = decoder(torch.as_tensor(X_end).float())
    # compute the difference between transformed representations
    X_diff = scale_transform(X_hat_end) - scale_transform(X_hat_start)
    return X_diff

def pairwise_feature_importance(
    data: DataFrame,
    weights: Union[List[NDArray], NDArray],
    group_by: str,
    feature_columns: str,
    labels: Union[List[Tuple[str, str]], Tuple[str, str]]
) -> NDArray:
    if not isinstance(weights, list): weights = [weights]
    if not isinstance(labels, list): labels = [labels]
    xs = np.empty((len(labels), len(feature_columns)))
    for i, (W, class_names) in enumerate(zip(weights, labels)):
        class_name_i, class_name_j = class_names
        class_idx = (data.loc[:, group_by] == class_name_i) | (data.loc[:, group_by] == class_name_j)
        X = data.loc[class_idx, feature_columns]
        feature_importance = abs(X.std(axis=0) * W.squeeze())
        xs[i] = feature_importance / feature_importance.sum()
    return xs

# ------------------------- Audio Transforms ------------------------ #

def frame_to_seconds(
    frame_length: int,
    sample_rate: int,
) -> float:
    return frame_length / sample_rate

def seconds_to_frame(
    duration_seconds: int,
    sample_rate: int,
) -> int:
    return int(round(sample_rate * duration_seconds))

def num_samples_to_num_spectrogram_frames(
    num_samples: int,
    sample_rate: int,
    hop_length: int,
    hops_per_frame: int,
    resample_rate: Optional[int] = None,
) -> int:
    resample_rate = resample_rate or sample_rate
    sample_per_frame = (hop_length * hops_per_frame)
    return ((num_samples / resample_rate) * sample_rate) // samples_per_frame

def fft_length(window_length: int):
    return 2 ** int(np.ceil(np.log(window_length) / np.log(2.0)))

def hertz_to_mel(
    frequencies_hz: NDArray,
    scaling_factor: float = 1127.0,
    break_frequency_hz: float = 700.0,
    log: Callable = np.log,
) -> NDArray:
    return scaling_factor * log(1 + (frequencies_hz / break_frequency_hz))

def mel_to_hertz(frequency, scaling_factor, break_frequency):
    return break_frequency * (10.0 ** (frequency / scaling_factor) - 1.0)

def stft_hop_length_seconds_to_feature_sample_rate(stft_hop_length_seconds: float) -> float:
    return 1.0 / stft_hop_length_seconds

def centroid_and_nearest_neighbour(data, labels, group_by, feature_columns):
    points = []
    centroids = []
    for label in labels:
        label_idx = data.loc[:, group_by] == label
        coords = data.loc[label_idx, feature_columns]
        centroid = coords.mean(axis=0)
        idx = ((coords - centroid)**2).sum(axis=1).argmin()
        centroid = centroid.rename(lambda col: f"{col}_centroid")
        point = data.loc[label_idx].iloc[idx]
        point = point.append(centroid)
        points.append(point)
    return pd.concat(points, axis=1).T

def interpolate(model, weights, z_i, z_j, delta=0.001):
    c_i = model(z_i.T)
    z_tilde = z_i.T.copy()
    # calculate starting distance to other class centroid
    d_start = np.sqrt(np.sum(np.power(z_tilde - z_j, 2)))
    # iterate until the class changes
    while (c_j := model(z_tilde)) == c_i:
        z_tilde += delta * weights
        # calculate new distance to other class centroid
        d_i = np.sqrt(np.sum(np.power(z_tilde - z_j, 2)))
        d_j = d_i
        # while distance is decreasing, keep interpolating
    while (d_j - d_i) >= 0:
        d_j = d_i
        z_tilde += delta * weights
        d_i = np.sqrt(np.sum(np.power(z_tilde - z_j, 2)))
        # fallback one so we revert to smallest distance
    else:
        z_tilde -= delta * weights
        d_i = np.sqrt(np.sum(np.power(z_tilde - z_j, 2)))
    return z_tilde.T

def str_to_enum(str_, enum):
    """
    Convert a string to an enum based on name instead of value.
    If the string is not a valid name of a member of the target enum,
    an error will be raised.

    :param str_: String to be converted to an enum member of type ``enum``.
    :param enum: Enum class to convert ``str_`` to.

    :returns: The enum member of type ``enum`` with name ``str_``.

    :raises TypeError: if the given string is not a valid name of a member of the target enum
    """""
    if isinstance(str_, enum):
        return str_
    try:
        return enum[str_]
    except KeyError:
        valid_ls = [mem.name for mem in enum]
        raise TypeError(f"'{str_}' is not a valid option for enum '{enum.__name__}'; must be one of {valid_ls}.")
